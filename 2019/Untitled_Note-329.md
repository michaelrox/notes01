# Untitled Note

`2019 misc`

Karl Gilis at YoastCon 2015

Wednesday, 12 June 2019

4:20 PM

a/b testing:

* Make a Solid Hypothises:
    * By changing x into, y I can get more prospects to z and thus increase abc
        * Michael Aagaard
* Don't test everything
* Don't test conversions to the next step. Test to the checkout instead.
* Example:
    No slider. Instead put all image banners in 1 area which is as big as the slider anyway.
    * This got 28% more clicks for Suzuki
    * Next test was to change to just 2 images instead of 4
* Remove clutter.
    * Make 1 clear button.
    * Make reassuring message in green
* Don't give customers big chances to remove items from cart
* 13.20min: Removing main navigation for AdWords visitors resulted in significant lift in bookings. On a travel website.
    * This is because people who search for something specific, don't need links to the about the company etc.
    * Make the navigation a little dropdown instead or something
* Bullet point lists always work
    * Too much copy puts people off
    * Show differences in bullets
    * This made 78% more clicks on Suzuki
* Add some reassuring text next to CTA
    * In Stock \- immediately available
    * Make sure copy has positive words, and NO negative words
* Nothing will work on every website
    * Green Check marks aren't always great. Sometimes best practices don't always work.
    * Having text like "More Info" on product images will help
        * Customers don't always understand that they can click an image/box
* Adding a 2nd CTA at the bottom will ALWAYS work
* Big useless photos at the top of the page are dangerous
* If asking for a detail, mention why. Give an explenation why you need their email address
* Showing product category will almost always work better than individual products.
* Ask:
    * Why are you on this website?
    * What made you buy this?
    * What made you not buy this?

 

 

 

 

 

 

Created with Microsoft OneNote 2016.
