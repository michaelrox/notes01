# 2nd Feb meeting

* My work so far has been satisfactory, in fact Just said they are very pleased with my work \(Last Monday \- 22nd.\)
* I quote time well and accurately
* Find Product task had no due date, it ended up needing an extra 8 hours.
* I am passionate about what I do, I do love working here
    * But \- work life balance is important to me
    * I tend to lose my social skills when put under stress or pressure.
* I understand that often unexpected tasks will arise, but I do my best to avoid this happening by working hard at work and planning far ahead.
