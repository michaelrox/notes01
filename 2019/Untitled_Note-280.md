# Untitled Note

`Mutt`

31 May 2019 14:42

Friday, 1 November 2019

10:17 AM

Michael Hiley

Shared on Website · 31 May 2019 14:42

Hey @Tim and @Danny

I've installed Hotjar on the website, Tim should have been sent an account invitation. This will start recording some data, so hopefully we can take a look on Monday. Tim will have the owner permissions in Hotjar and can create more accounts.

I've also integrated Shopify with Zoho, this may take a couple of days to sync up. This should sync all contacts and event data.

Let me know your usename/password for Mailchimp and I can integrate that as well. \(Happy to run through the process over the phone if you don't want to give out passwords.\)

LikeCommentCreate a task

Show previous comments3/4

Michael Hiley

@Danny FYI the open hours are also updated on the site now [https://muttmotorcycles.com.au/pages/get\-in\-touch](https://muttmotorcycles.com.au/pages/get-in-touch)

I also added the hours to the footer, it's pretty out of the way, but I don't think it needs to be any more prominent.

Get In Touch

Link · 0 views

Monday \- Saturday: 9:00am \- 5:00pm

Like

· 31 May 2019 15:53

Michael Hiley

I'll need you to update the hours Google Business side

Like

· 31 May 2019 16:00

Danny

No worries, thanks @Michael Hiley

Like

· 1 Jun 2019 8:04

 

Created with Microsoft OneNote 2016.
