# One Three

Branding

Pastel

pastel beige: \#ffb3a9

pastel pink: \#f2a9ff

pastel green: \#befcb4

pastel purple: \#6f6caf

Dark

black: \#0f141a

murky green: \#656531

gold brown: \#b58a3c

Original

bone: \#ffedc5

other pink: \#ffbafe

mint green: \#a4e7c3

light blue: \#baf7ff

dark blue: \#4f81b8

```
<div style="display: none;">
   <ul>
      <ul>
         <li><b>Pastel</b></li>
         <li><span class="swatch" style="background-color: #ffb3a9"></span>pastel beige: #ffb3a9</li>
         <li><span class="swatch" style="background-color: #f2a9ff"></span><b>pastel pink: #f2a9ff</b></li>
         <li><span class="swatch" style="background-color: #befcb4"></span>pastel green: #befcb4</li>
         <li><span class="swatch" style="background-color: #6f6caf"></span>pastel purple: #6f6caf</li>
      </ul>
      <ul>
         <li><b>Dark</b></li>
         <li><span class="swatch" style="background-color: #0f141a"></span><b>black: #0f141a</b></li>
         <li><span class="swatch" style="background-color: #656531"></span>murky green: #656531</li>
         <li><span class="swatch" style="background-color: #b58a3c"></span>gold brown: #b58a3c</li>
      </ul>
      <ul>
         <li><b>Original</b></li>
         <li><span class="swatch" style="background-color: #ffedc5"></span><b>bone: #ffedc5</b></li>
         <li><span class="swatch" style="background-color: #ffbafe"></span>other pink: #ffbafe</li>
         <li><span class="swatch" style="background-color: #a4e7c3"></span>mint green: #a4e7c3</li>
         <li><span class="swatch" style="background-color: #baf7ff"></span>light blue: #baf7ff</li>
         <li><span class="swatch" style="background-color: #4f81b8"></span>dark blue: #4f81b8</li>
      </ul>
   </ul>
</div>

<style>
   .swatch{
      height: 30px;
      width: 10px;
      display: inline-block;
      float: left;
      margin-right: 10px;
   }
</style>
```
