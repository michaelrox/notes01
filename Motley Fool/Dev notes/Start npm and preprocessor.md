# Start npm and preprocessor
```
cd ~/Containers/Fool-AU
lando start
cd public/wp-content/themes/freesite-2020-theme
npm install
npm run build
npm run watch
```

if npm needs an update, then try:
---

![](https://ca.slack-edge.com/ETFE6684X-W018UD6UYTA-g2f57954f603-48)

[Aaron Shaw](https://app.slack.com/team/W018UD6UYTA)  [10/9/21 12:02](https://fool.slack.com/archives/D012JQRP9DW/p1631239356007500)  

Updating npm required me to run/install this :

``` xcode-select --install ```