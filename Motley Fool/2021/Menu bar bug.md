AU staging has the same problem, though CA staging doesn't.
I've gone through AU staging and disabled all custom code and the problem persists.

Problem across sites
Started around 30th August
currentColor problem for border - though it's just color in the other areas
 `.header-bar a` is being over-written by `body a:hover` 
 
 **Cache**
 no - tested clearing Nitro and WPEngine...

**CSS**
no - combed through scss files. No changes for a month, no significant changes ever...

**Mega Menu settings?**
freesite 2021 - Imported (Max Mega Menu Location 1)
Custom Styling: There is a lot of custom styling, I removed all and the problem persists. Noting that accordion icons show up on home page as well though. So the fixes in here fix bugs outside the mega menu.

Freesite Default:
Custom Styling: Removed it all, and the button completely dissapeared.. I could be on to something.