# Cheesecake

### **INGREDIENTS**

1 x 250g pkt plain sweet biscuits \(such as Arnott's Nice\) 👍

125g unsalted butter, melted👍

750g cream cheese, at room temperature👍

215g \(1 cup\) caster sugar👍

1/2 teaspoon vanilla extract👍

2 teaspoons finely grated lemon rind 👍

2 tablespoons plain flour

4 eggs

1 x 300ml ctn [sour cream](https://www.taste.com.au/search-recipes/?q=sour%20cream&ct=recipes)

120g fresh raspberries or frozen 

Place the biscuits in the bowl of a food processor and process until finely crushed. Add the butter and process until well combined. Transfer to the lined pan. Use a straight\-sided glass to spread and press the biscuit mixture firmly over the base and side of pan, leaving 1cm at the top of the pan. Cover with plastic wrap and place in the fridge for 30 minutes to chill.

**Step 3**

Meanwhile, use an electric beater to beat the cream cheese, sugar, vanilla and lemon rind in a large bowl until just combined. 

Beat in the flour.

Add the eggs, 1 at a time, beating well after each addition until combined.

Stir in the sour cream until just combined.

**Step 4**

Pour the cream cheese mixture into the base. Place the pan on a baking tray and bake for 1 1/4\-1 1/2 hours or until just set in the centre. Turn oven off. Leave the cheesecake in oven, with the door ajar, for 2 hours or until cooled completely \(this will prevent cheesecake from cracking\). Place in the fridge for 4 hours to chill.

**Step 5**

Top the cheesecake with the raspberries. Cut into wedges to serve.
