# Jobs to apply for

[https://jobsearch.gov.au/Job/View/Details/2270793784?context=a5360307\-6476\-6f89\-7e81\-c01d9d4319a4](https://jobsearch.gov.au/Job/View/Details/2270793784?context=a5360307-6476-6f89-7e81-c01d9d4319a4)

**Website Digital Manager**
CKY Media provides web and graphic design services to a very diverse range of clients. We are a small, friendly team working out of an awesome office space in Subiaco.

From a technical point of view you will be ensuring best practices are applied across our web projects \(code standards, scalability, performance, etc.\). **You won't actually be doing intensive coding�**�but you will be helping to oversee quality control and assisting in estimating project timelines and development time for quotes.

From an account management point of view you'll be liaising with our clients adding value to our projects all the way from the quoting stage through to successful project delivery and managing recurring work.

**What you'll need to get the job:**

* Advanced knowledge of WordPress \(WooCommerce would also be an advantage\)
* Proven record of managing a diverse range of projects for end clients
* Knowledge of web strategy, design, and emerging digital trends
* Outstanding communication and interpersonal skills \(written and verbal\)
* Ability to coordinate multiple projects with competing deadlines
* Attention to detail is a must and eye for design would be an advantage
* Be a permanent Australian resident. 

 

**APPLY NOW** by calling Rebecca on 0450 995 926 or [Becky@ckymedia.com.au](mailto:Becky@ckymedia.com.au)

---

Senior and Junior
[http://www.seek.com.au/job/29991942?savedSearchID=12352294&tracking=JMC\-SAU\-eDM\-JobMail4.01\-3881](http://www.seek.com.au/job/29991942?savedSearchID=12352294&tracking=JMC-SAU-eDM-JobMail4.01-3881)

# Project Manager x 2 \(Junior with Design Skills and Senior Business Processes\)

We currently have two exciting positions available for a January start.

These positions are with an Australian leader in Oracle and Cloud Solutions based on the northern outskirts of Perth CBD. These are full\-time permanent positions with the opportunity to work within a fantastic, supportive and social team.

_**Senior Project Manager**_

This position has a key focus of managing projects through the complete ICT Project Lifecycle. This entails working with clients in a business analyst capacity then project scoping and management and transitioning out.

Competencies 

* Previous experience in a business role such as Finance, Procurement or Business Planning and Improvement. A focus on customer service is highly desirable
* Analysis and problem solving
* Excellent spoken and written communication including report writing, presentations and negotiation.
* Ability to work without regular supervision
* Project Management
* Well\-developed interpersonal skills \(team player, relationship building, flexibility, etc.\)
* The position will have a 20\-30% travel requirement

_**Junior Project Manager**_

The position has a key focus of managing less complex projects or contributing to larger more complex ones in the context of the ICT Project Lifecycle. This includes working with clients in a range of tasks such as business analyst capacity then project scoping and management and transitioning out.

Competencies 

* Some experience in a business role such as Finance, Procurement or Business Planning and Improvement. A focus on customer service is highly desirable
* Analysis and problem solving of moderately complex issues
* Well\-developed spoken and written communication, report writing, presentations and negotiation. 
* Ability to work without regular supervision or as part of a team
* Project Management of less complex projects
* Good interpersonal skills \(team player, relationship building, flexibility, etc.\)
* Other notes:
* The position will have a 20\-30% travel requirement
* Candidates with a graphic/web design or UX background looking to move into a business systems role would be highly regarded \(Skills in PHP, HTML, CSS, JavaScript looked favourably upon\)

These roles are excellent opportunities to further your career with solid projects within a great company and team. Attractive salaries and flexibility with work \(although these are full time roles\!\) are available.

To apply please email your CV through the SEEK prompts below directed to Celestine @ The Ruby Group. Also, please state which role you are interested in applying for \(Senior or Junior\).

Apply today for a mid January start\!
