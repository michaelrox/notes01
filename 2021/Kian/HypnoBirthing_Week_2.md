# HypnoBirthing Week 2

Homework: Continue to:

* Read HB book
* Listen to all mp3s

Also:

* Review Birth preferences
* Prepare emotion & fear release worksheet
* Facial relaxation
* Progressive Relaxation
* 3 breaths
* Partner reads Rainbow and Birth Companions reading
* Light touch massage
