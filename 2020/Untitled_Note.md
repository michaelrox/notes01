# Untitled Note

**$500 each:**

**AVH\(AVITA MED. LTD.\)**

**TWE\(TREASURY WINE ESTATE\)**

**Others** for the future:

ETHI

VESG

**$4,500**

**$1,000: VESG👍**

**$1,000: VGS👍**

**$1,000: AEF👍**

**$500: AVH👍**

**$500: TWE👍**

**$500: katmandu👍**

Predictions:

I predict prices will return to same as they were at the end of Feb within 12 months.

|Stock|20/02/2020|20/04/2020|Predicted growth %|
|-----|----------|----------|------------------|
|     |          |          |                  |
|TECH |$92.45    |$85.2     |8.50939%          |
|VGS  |$88.14    |$76.5     |15.2157%          |

![image.png](image/image.png)
