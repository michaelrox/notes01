# CALIGUEL LAGOONS

[https://goo.gl/maps/wRVcWjrtAR5hpV8W7](https://goo.gl/maps/wRVcWjrtAR5hpV8W7)

4:30 hr drive

north of bris

This is a large, free overnight rest area that has a boat ramp into the lagoon. Surrounded by shady Eucalyptus trees, you will find gravel sites that are fairly level to park on. 

Here you will find toilets, cold shower, non\-drinking water, picnic tables, BBQ fireplaces and pets are allowed. You will need to bring your own drinking water and firewood.

It is quite a popular water sport and fishing area.

bird watchoing

### **ADDRESS**

Condamine\-Meandarra RoadCondamine, Queensland, 4416
