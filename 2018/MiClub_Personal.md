# MiClub Personal

\- There is a very negative vibe in the creative team, everyone feels the same.

     The situation is 100x better with a manager now, but I don't think it is good enough that we now "know what we are doing within a weeks time" and nothing else gets resolved.

Personal

\- Pay is something I accepted at the start as I was new to a team environment and I understand it was hard to judge me. Being here for a year and not having any sign of a progress meeting implies that MiClub will not ever have intentions of assessing my new skills, let alone offering a pay rise based on these skills.

\- I also accepted a lower pay under the assumption that I would be working with a highly efficient team who I could learn from and gain experience worth paying for.

\- When freelancing I was proud of every project I created, I feel like I haven't made anything to be proud of yet, and can't see that opportunity arising in the future.

         \- Part of this is because we are treating clients and our sales\-reps like they have the same technical knowledge that we do

Miclub:

\- MiClubs not offering anything to make me feel passionate about my job

\- Hiring someone purely for design when we are flooded with development work

\- No\-one in creative was involved in this hiring process \(apart from Paula in the final stages.\)

\- I brought up a whole bunch of problems in the past, these problems are yet to be resolved. It wouldn't suprise me if I were to bring them up again and Peter would think they are brand new problems.

\- Senior members of the company don't bother trying to find solutions, which it makes me wonder why I should even bother notifying them/anyone of the problems.

\- Senior members and Directors don't pay attention to advice given and are too happy to accept the lowest of standards.

\- I already know how to manage my own projects, my goal in working as part of a team is to learn how professionals manage/work so that I can improve myself.

\- It feels like the blame is being put on us for not having enough time to do our work.

\- Peter told us he wants us to enjoy where we work, and yet has continued to ignore anything that has been brought up in meetings in the past.

I'm not sure how to fix the current state of things.

Problems with development:

\- The projects take too long to get deployed to a stage ready for work

\- By this stage the website is built on a base of bad hacks to make things work, instead of building on a strong foundation which simplifies the workload down the road.

\- I would be happy to spend the time rewriting the code, but the structure is so confusing and such a large mess of intertwining layers that it would take a week or two find a starting point.

\- The knowledge roof here is high in areas which don't interest me.

\- There isn't an excuse to have code all over the place just chopped and slapped in to place as it is.

     \- Because of this, nothing is going to be fixed and we are going to create more websites with this terribly low standard

\- QA should be taken more seriously.

     \-  UAT is what we think we call QA, which is User Acceptance Testing.

         \- UAT is the point where the user/client is happy with the website, from here our team should be performing another QA which goes through a number of _hidden_ tests to ensure the website is top grade in terms of: SEO, Page loading time, HTML/CSS symantics, etc.

\- QA should be enforced on essential themes and plugins

\- Andy has given me a valid excuse as to why we can't have a more efficient system, and because of this no\-one has asked questions or bothered looking for solutions. Looking for solutions should be something which at least one team member is spending 30mins every day doing. 30mins a day is far less than the amount of time we waste using what we do.

Plugins

\- We can't promise anyone that a plugin will look exactly similar to their current theme if we aren't given the time to do so.

     \- It is great that we estimate/quote time for Custom sites \(which still hasn't been done from what I have seen\) but we should also individually quote time \(not price\) for plugins and Essentials in order to manage customer expectations.

Problems with projects

\- A non\-creative is still quoting delivery times.

\- All projects have fixed prices but extremely variable modules

\- Sales team are showing clients old, outdated websites as examples, and then telling the client that things are possible which actually aren't... Just because we have done something once in the past, it doesn't mean that it works and is good, obviously I'm referring to things which were badly done and caused us problems down the road.

Product...

\- No customisation options and yet it is frequently requested

\- Backend team makes it hard to get anything changed \(Which I understand since they need to make a solid product, but this is the answer to why we can't do a lot of requests\)

Dev solutions...

\- Andy and Paula need to run through what makes our system have to be done in such a strange way

\- Get it down in writing, with each step

\- Front\-End members can spend a little time a week to find a different solution for as many steps as possible.

WordPress

\- We should be using a single theme as the Core theme

     \-  all websites are child themes which can be updated... Google it if you don't understand what this means.

\- I don't enjoy my work
