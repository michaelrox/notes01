# 2015 June - July: Bali KL

19 June:Land in KL at around 4:30. 
We got a cab for around 200 MLR \($50aud\) and stayed at the Royale Chulan hotel 

20th June:
FRIM canopy walk, we got there around 8:30 and the walk doesnt open until around 9:30. 

We did the big walk up the mountain anyway, about 1.5km steep and very hot, I made the mistake of wearing jeans. 

From here we went to the Batu caves and walked up the steps. Lots of monkeys along the way, and very impressive caves up the top. 

China town for lunch, and we were pretty tired by then so headed back to the hotel for a much needed shower before the second half of the day. 

Jalan Alor for dinner, great street and we snuck in another drink after around the corner at the Mango bar. 

21st June. 

Jalan Alor for brekky, went for vietnamese. 

Pavillion shopping center, farenheit 88 shops

Petronas towers, we just walked around the public area, saw more than enough. 

Pisco Tapas for dinner. Helipad Lounge for a drink after... Got a pretty big frieght after realising i was sitting on top of a building having a cocktail and there were no barriers. The live band on the roof was right on the edge. 

22 June. 

Off to Bali, spending the last of our money on a cab to the airport, and having a egg mcmuffin each for brekky. 

\-\-\-\-

Bali \- 

Hotel cab wasnt able to pick us up So we got a cab for $20 to Mercur Sanur. 

Started the yoga retreat at 4:30pm with a class focusing on breathing, including some laughing. 

Saw mum and dad at 7 at the Bamboo Bar before going to dinner at the beach club restaurant.  

23 June. Tuesday

7:30 yoga

 \- Malaika Secret Garden for Brekky

\- I got a massage, Chantelle stayed in hotel 

The Glass house for lunch

Looked at a few shops with mum and cj 

 4:30 Vinyasa flow

Cafe jepun for dinner with mum and dad and cj
