# 90day callenge

90daysSprint 1: 14 sep\- 13 Oct
Recovery: 14 Oct\- 18 Oct

Sprint 2: 19 Oct \- 17 Nov
Recovery: 18 Nov \- 22 Nov

Sprint 3: 23 Nov \- 22 Dec

Goals:
Over all: 

1. Weight loss
2. Yoga/hand balancing
    1. Floating with knees bent
    2. Free Handstand
    3. Forward fold with nose on knee
3. Good daily habbits
4. Call someone I miss once a week
5. Wooden box
6. Golf lessons
7. Ukelele

Method:

1. 5/2 diet
    * Cheat meal once a week
2. 1 hr yoga/bodyweight workout a day 
3. Daily habbits:
    1. Teeth brush and floss
    2. Fish oil tablet
    3. Track foods

Sprint 1 goals: 14 sep\- 13 Oct
\(4 weeks\)
Current weight: 88kg
\-0.5kg a week = 86kg

1. Lose 2kg
2. Yoga:
    1. freestanding pike for 10 seconds
    2. Wall handstand for 60sec
3. ...
4. ...
5. Plan wooden box
6. Suss out golf lessons
7. Learn angel on uke 100%

Sprint 2 goals

Weight: 86\-\> 84

    1. Yoga:
        1. freestanding pike for 10 seconds
        2. Wall handstand for 60sec
    2. ...
    3. Track food\!
    4. Plan wooden box
    5. Suss out golf lessons
    6. Learn angel on uke 100%

Sprint 3 goals
