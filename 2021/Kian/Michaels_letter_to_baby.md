# Michaels letter to baby

`hypnobirthing`

I want you to know:

I am already in love with you.

I'm enjoying this pregnancy journey.

I've been looking forward to this journey for a long time.

You are going to help Chantelle with her pregnancy.

Chantelle is having a wonderful pregnancy.

Chantelle will have a blissful labour and birth.

Your life has brought abundance of joy in to our lives.

I accept all of the emotions that you bring upon me.

I accept all of the experiences which you will bring.

I'm excited to watch you learn and grow in to your own.

I will always guide you towards safety.

I promise to listen to you.

You will always have love from myself and Chantelle.
