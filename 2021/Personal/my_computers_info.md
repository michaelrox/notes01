# my computers info

PC case:

SilverStone Precision PS08 Black mATX Case

**Dimension**168mm \(W\) x 355mm \(H\) x 400mm \(D\)

6.61" \(W\) x 13.98" \(H\) x 15.75" \(D\)

CPU Intel Core i5 6500 Skylake 3.2GHz 6MB Retail Box

CPU Cooler Cooler Master Hyper TX3 EVO CPU Cooler

Motherboard Gigabyte B150M D3H LGA1151 DDR4 mATX Motherboard

Memory GeIL 8GB Single DDR4 Pristine C15 2400MHz

OS Drive GeIL Zenith A3 120GB 2.5" SSD

Storage Drive WD Blue WD10EZEX 3.5" 1TB 64MB 7200RPM Desktop HDD

Graphics Card Palit GeForce GTX960 4GB GDDR5

Card Reader None

Optical Drive None

Sound Card Integrated \- Onboard

Case SilverStone Precision PS08 Black mATX Case

Power Supply FSP Raider Edition 750W 80PLUS Silver Power Supply

Wireless None

Operating System Microsoft Windows 10 Home OEM 64\-Bit DVD

Office Suite None

Antivirus ESET Smart Security 1 Year OEM

Monitor Asus VS248H 24" Full HD WS 2MS LED Monitor

Keyboard None

Mouse None

Speakers None

Mouse Mat PLE & EpicGear Hybrid Mousemat Small

Power Protection Power Shield ZapGuard 6 Way Surge Board Black

Warranty 24 Month PLE Return to Base Warranty

Assembly PLE Professional Assembly and Testing

$1632.00

**2015**

**2020**

GeIL 16GB Kit \(2x8GB\) DDR4 EVO SPEAR C16 2400MHz \(GSB416GB2400C16DC\)

$129.00

PLE\-634511 Samsung 860 QVO 7mm 2.5" 1TB SSD 76Q1T0BW

$195.00

PLE\-624837 Kingston 8GB DDR4 ValueRAM C17 2400MHz \(KVR24N17S8/8\)

$89.00
