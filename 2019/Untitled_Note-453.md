# Untitled Note

`Deus Ex Machina`

Current site

Wednesday, 13 February 2019

6:09 PM

 

* Shopify
* International redirect
* Website navigation:
    * Shop online
        * America
        * Australia
        * Europe
        * Japan
        * International
        * ~ 1000\+ products
    * About
        1. Stockists
            1. ~ \+500 stores
        2. Contact
        3. Terms
        4. Privacy
    * Stores
        1. Sydney
        2. Bali
        3. Tokyo
        4. LA
        5. Italy
        6. France
        7. Bali
        8. Byron
        9. NSW
        10. NSW
        11. Madrid
        12. Bordeaux
        13. Zurich
    * Bikes
        * Motorcycles
            * ~\+50 custom bikes with unique pages/articles
        * Bicycles
            * ~\+14 custom bikes with unique pages/articles
        * Builders
            * 2 builders
        * XJR\-X kit
            * 1 page, with eoi form
        * 400 kit yard
            * 1 page, with eoi form
        * New yamahas
            * 1 page
    * Surf
        * Rubber
            * 1 page
        * Boards
            * ~\+50 custom boards with unique pages/articles
        * Shapers
            * 15 user profiles
    * Cafes
        * Each has unique page, sometimes options to book online or deliveroo etc.
        * Sydney
        * Bali
        * Harajuku
        * Venice
        * Milan
    * Records
        * Website
            * Seperate website, has option to buy online
        * News
            * Same central blog
    * News
        * Same central blog
    * Media
        * Big list of videos
    * Blog
        * Same central blog

 

Created with Microsoft OneNote 2016.
