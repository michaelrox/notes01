# PLP brainstorm

123

Quick View:

1. Need swatches to show
2. Need sold out / add to cart to work.
3. This is built using clunky AJAX at the moment

Products on Collections

1. Need to show each variant
2. Need to have image slider
3. Need Swatch to link to slider image
    1. [https://www.reddressboutique.com/collections/all\-dresses](https://www.reddressboutique.com/collections/all-dresses)
        [https://www.urbanoutfitters.com/womens\-new\-arrivals](https://www.urbanoutfitters.com/womens-new-arrivals)
        Examples:
4. Built with clever Liquid at the moment
5. Clicking size should quick add to cart.

Solution.

1. Bring in all variants on Collection
2. Bring in full image slider
    1. link swatches to images
    2. Cross out sizes that are sold out

full image slider.

1. update existing product\-item.liquid 
    1. Currently pulls in 3 images, but may have to bring in all and fore\-go lazy loading
2. Some how recreate entire PDP \(this will link swatches to images...\)
