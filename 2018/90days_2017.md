# 90days 2017

**Schedule**:

Mon

* Workout

Tues

* Study
* Run

Wed

* Workout
* Clean/Shave

Thurs

* Study
* Run

Fri

* Workout

**Sprint 1:** April 18 \- May 23

* Trial doing stuff in the Morning

**Recovery:** May 24 \- May 28

**Sprint 2:** May 29 \- June 27

* Trial doing stuff in the afternoon

**Recovery:** June 28 \- July 2

**Sprint 3:** July 3 \- August 1

**Goals:**

Work

* 6 hours a day

Study JavaScript

* Twice a week\+ \(Tuesday and Thursday?\)

Workout

* Three times a week \(Mon, Wed, Fri\)

Run

* Twice a week \(Tuesday & Thursday?\)

Clean home/me

* Shave/groom myself
* clean or car

**outcomes**:

90% habitual.

10% the gainzzz

**gainz**:

Work: Log times, get focused, track things I have done. Spend Recovery time showing how productive I am to others.

Study: Finish the current tutorials and build a basic app, start more advanced app maybe

Workout: Hold handstand for 60seconds against wall, wrist strength in general

Run: 5km without stopping, 10km without stopping?
