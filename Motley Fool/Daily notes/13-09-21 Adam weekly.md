## Done:
- Source codes in Athena - slack channel fixed it quickly.


## Ongoing:
**Smallcaps**
Portfolio Services, Ultimate Growth, kind of like Pro.
23rd of Sept.

**Star Ratings**
Working through presentation - schema works but it needs to display text on front end as well.

**New Report Pop up**
Need feedback from the user

**Allocation Calculator**
Sent design to Adam and Erin for feedback

**Service Scorecards Redesign**
Yet to begin - I'll mockup properly since I have a few ideas:
- Mobile first
- New theme UI components
- Prepare for a *merged* service (a dream for now.)

**Investing Education page**
Matt mocked up a page in Beaver Builder, I'll build it out.
Estimate half a day

**Instagram**
Since start: +2.6% followers. (500)
currently at 18.4k

**NAS**
Works from within the office 

**Guide to investing**
Scott recorded everytihng, audio was bad and it needs a full redo.

**Need to apply for leave**


## Up Next
**Fool Premium Hub**

