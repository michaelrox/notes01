# theme things to add

is\_sticky\(\)

[https://codex.wordpress.org/Function\_Reference/is\_sticky](https://codex.wordpress.org/Function_Reference/is_sticky)

.screen\-reader\-text

.screen\-reader\-text:focus

* WARNING: The tag **responsive\-layout** has been deprecated, please remove it from your style.css header.

Theme URI and Author URI cannot be the same, which comes to..

users can bypass this if use a "/" at the end of a uri.

theme uri: [http://www.domainname.com](http://www.domainname.com/)

author uri: [http://www.domainname.com/](http://www.domainname.com/)

"

REQUIRED: **get\_currentuserinfo\(\)** found in the file **admin\-bar.php**. Deprecated since version **4.5**. Use **wp\_get\_current\_user** instead.

REQUIRED: **get\_bloginfo\('template\_directory'\)** was found in the file **functions.php**. Use **get\_template\_directory\_uri\(\)** instead.

REQUIRED: **get\_bloginfo\('template\_directory'\)** was found in the file **footQ.php**. Use **get\_template\_directory\_uri\(\)** instead.

* RECOMMENDED: No reference to **add\_theme\_support\( "custom\-header", $args \)** was found in the theme. It is recommended that the theme implement this functionality if using an image for the header.
* RECOMMENDED: No reference to **add\_theme\_support\( "custom\-background", $args \)** was found in the theme. If the theme uses background images or solid colours for the background, then it is recommended that the theme implement this functionality.
* RECOMMENDED: No reference to **add\_editor\_style\(\)** was found in the theme. It is recommended that the theme implement editor styling, so as to make the editor content match the resulting post output in the theme, for a better user experience.
* RECOMMENDED: Could not find the file **readme.txt** in the theme. Please see [Theme\_Documentation](https://codex.wordpress.org/Theme_Review#Theme_Documentation) for more information.

INFO: **templates/page\-members.php** The theme appears to use include or require. If these are being used to include separate sections of a template from independent files, then **get\_template\_part\(\)** should be used instead.
