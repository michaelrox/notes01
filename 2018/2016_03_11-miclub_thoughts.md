# 2016 03 11 - miclub thoughts

Dan G is a good example of knowing a lot of code but not knowing how to correctly apply it. After having to use his code it has become obvious that he doesn’t fully understand how and when to use css. Michal and Paula had the same problem with html/php.

I am starting to understand that these differences is where I am excelling. 

e.g. incorrect usage of position: absolute, and z\-index, and none\-flexible image sizes.
