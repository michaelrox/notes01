# delux qa 02

Change properties in System \> Properties

1. * wordpress.home.enabled = true
    * site.portal.version = 3

Set a static page as the home page \(Wordpress Dashboard \> Settings \> Reading \>_Front Page Displays_\)

[http://deluxe.miclub.com.au/cms/](http://deluxe.miclub.com.au/cms/)

\- slider image looks pixelated \- I think it is the image itself

[http://deluxe.miclub.com.au/cms/page\-formats/regular\-page/](http://deluxe.miclub.com.au/cms/page-formats/regular-page/)

[http://deluxe.miclub.com.au/cms/about/](http://deluxe.miclub.com.au/cms/about/)

broken image

login dropdown overlaps menu bar

![chrome_2017-02-28_11-35-34.png](image/chrome_2017-02-28_11-35-34.png)

Public Bookings styling:

[http://deluxe.miclub.com.au/guests/bookings/ViewPublicCalendar.msp](http://deluxe.miclub.com.au/guests/bookings/ViewPublicCalendar.msp)

\- login dropdown![chrome_2017-02-28_11-20-51.png](image/chrome_2017-02-28_11-20-51.png)

\- space for sidebar but no sidebar \- maybe this gets fixed with some bookings?

Reciprocal Clubs

[http://deluxe.miclub.com.au/cms/golf/reciprocal\-clubs/](http://deluxe.miclub.com.au/cms/golf/reciprocal-clubs/)

accordion keeps dancing if you click lots
