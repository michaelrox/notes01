# prep

Accomplishment/s     \- Sass/bootstrap integration with our java back\-end
     \- Convincing a project manager is a good idea

Difficult
     \- Clients consistently requesting large design changes after agreeing on a design and after things have been built.
          \- Overcome by integrating design sign off form and convincing the director of the company that this will not affect our partnership, and if anything will give us a higher level of appreciation and respect.

3. 3 adjectives.
     \- Evolving, Organised, passionate
          \- team, modern, efficient, fun, happy, energetic, 

4. Specialization,  EDMs, direction

5. coworkes would say i am multitalented and helpful. While i prefer to stick to my own work im often the person to come to if someone doesnt know something. 

6. xxxx 
     \- Tracy was very rude and I let her know I don't appreciate to be spoken to in a disrespectful manner.
     \- Clients used to be very slow to supply website content and then get mad when we wouldn't be able to add the content. After a certian amount of time I let them know their project is on pause until I recieve the needed information, and their project will lower in my que until so.

7. I will let the person giving me the time frame the honest answer, clearly explain why, and give a potential solution for what I can accomplish within the time or how it would be possible.

8. Let them know why it is unrealistic and offer suggestions for other options

9. I am doing more support than I should be and the company has said they will resolve the problem but have not taken action, and forget whenever I remind them.

10. I want to work in a well organised team with career development opportunities. 

11. The best manager understood what they were asking in every task, if not they would ask for advice when needed. From here they would base expectations.
     The worst didn't put his employees before his clients and therefore stabbed himself in the foot more often then not. Having a manager who is short sighted like this lowers my motivation to perform well for them and makes me question the companies reasoning for hiring them.

12.
     \- It is OK to be firm so long as you are not rude and give a clear explanation.
     \- Extreme levels of confidence in WordPress and jQuery
     \- Always consider how things will be maintained and upgraded in the years to come.

13. Underquoted by assuming something was done the same way I would do it. I assumed a wordpress nav menu was made using the standard menu. Instead it was a series of included php files and the li's were images not text. This took a while to repair and I fixed it within the quoted time by using the same image hack, but to fully resolve the issue I called the client and let them know I can fix it for the future if they are happy to wait a few more hours. 

     \- I learned from this to always map out development work no matter how small the task.

14. I feel my after work study/interest and side projects are what make me successful and keep my work improving on every project.

15. I loved working for XXXX as I was personally interested in the product and the work environment was very friendly and encouraged social get together outside work hours.
Miclub has flexible working hours and a...

16. I have been succefull in both freelancing and team environments, Freelancing taught me a lot about self\-management, and I decided to work in a team environment because I like being around people and I believe I will always have more to learn in all areas and being in a team helps with bouncing ideas around.

17. Passion, Structure, Motivation, Commitment

18. I am looking to develop skills which will help progress my career in to a senior role. This would be across areas of management and leadership.

My questions:

Could you describe a typical week in this position?

What would be a typical task given to me if I were to be 3 months in to this role.

What process usually happens between a websites development and its deployment.

What kind of circumstances would result in the team staying back late.

Is there anything concerning you about my being successful in this position? Is there anything I could do to help

What is the next step in the process?
What is the usual time\-line?
