# Media Sphere

**Chat to Pete**

details only

* Thinking of kid, need to financially support.
* what is my potential over the next 5 years, considering if I moved departments etc
* been offered 75k plus super, plus working from home sometimes

---

scary \- unkown if it will be good or bad, or if I'll regret leaving Miclub forever...

What I want financially \- 

Start:

70k \- include super

12th June

**Reason to decline:**

\- taking a pay cut

\- not sure if I will get what I'm looking for

\- Chus Asensio \- it sucks because I really like his portfolio and would love the opportunity to work with someone and learn from someone like him

---

**Miclub pro/cons**

Pro

* chill
* low work expectation
* low hours
* friendly team 
* super flexible

Cons

* lonely
* no learning potential
* shitty tech restrictions.
* low pay

**MediaSphere Pros and cons:**

﻿
 Pro

* Different \- Not Miclub
* Growing company  \- potential for better role in future \- not really sure
* Git, Sass, Angular project in future  \- I would learn how a proper team does things...
* fun young office
* more pay
* team looks fun

Con

* 9hours of work time a day
* CEO is a bit of a tool, but it looks like he sticks to himself a lot

---

**Interview Prep:**

info:

SaaS \- Software as a Service \- dropbox, evernote, zendesk etc.

my questions:

* what is the typical task I would be doing?
    * what software/setup would be used currently to achieve that?
    * CMS Now? any restrictions?
* career progression \- would there be future opportunities for me to grow in to?
* what did they mean by flexible workplace arrangements?

before leaving:

* if not sure about how I went, ask if there is something they aren't sure about and if I could try to address it
* What is the next step or time frame in the process

me:

* UI Design/developer looking to take my career to the next level
* lots of jobs while studying \- design, html, tutoring, teaching, plus XXXX, and XXXX Graphics design at the end before going to Linz.
* past 2.5 years working and growing confidence in design and wordpress dev.
* 6months backpacking with wife, returned to same company to tie off some loose ends and settle back in easily.
* now ready to move on, find somewhere I can settle and be proud of working with.

Exuding confidence and interest in the job are two of the most important assets you can possess\! Go get the next interview\!

\-\-\-

---

Job description:

**Who We Are**

Mediasphere is an industry leader in the build and delivery of SaaS applications to manage online induction, compliance, performance talent and certification management. We are a small company with highly talented professionals and, in 2016, our software was awarded as a Global Top 10 LMS in the USA.

You will be working with a team of talented programmers, developers and designers. This is a new role as we expand our Design and UI Development Team. 

www.mediasphere.com.au | www.powerhousehub.com | www.inductnow.com 

WEB DESIGNER / FRONT END DEVELOPER \(HTML, CSS, SASS\)

**Who You Are**

Creator of clean, simple, yet blindingly brilliant code.

A good eye for design detail and high standards.

An insatiable appetite and excitement for new web technologies and techniques.

Good problem\-solving skills and the ability to think clearly and logically.

A strong desire to write well tested, maintainable, scalable code.

Willingness to take ownership of projects.

Your Technical Skills

Strong front end development skills and knowledge of current trends \(CSS, HTML and JS\)

Have a strong understanding of CSS pre\-processing platforms, preferably SASS

Experience with source control \(Git and/or SVN\).

Basic knowledge of PHP and MySQL

Knowledge of W3C web and accessibility \(WCAG\) standards.

Bonus points

Knowledge of AngularJS

Have a basic understanding of asynchronous request handling, partial page updates, and AJAX

Experience on design tools like Adobe PhotoShop, Illustrator and Sketch

The role is based out of our Southport Office in Queensland and we can also offer flexible workplace arrangements. The role could suit a recent highly skilled graduate or a UI Developer looking to take their career to the next level.
