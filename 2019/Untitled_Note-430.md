# Untitled Note

`Mizzie`

01 Theme research

Monday, 18 February 2019

12:38 PM

Hey Sandra,

Here's a list of themes that I think would work well with Mizzie, I understand if you would rather run through this over the phone.

 

I'll try to add some key points for why I chose each theme. Specifically:

* Optimized to run fast on all devices.
    * Score is out of 100, 60 is regarded as a very high mark with Shopify as Shopify has it's own methods of performance optimsation which confuse automated tests.
* Option to add related products on product pages
* Option to add a promotion banner
* Blog \(for tips, promotions, Mizzies world etc.\)

 

Motion / Minimal \(Periscopes choice\)

122 reviews \- 100% positive

performance rating: 46 / 100

* Design is modern and clean, but relies on good content.
    * Lots of options to add brand and product images and videos.Content must be high quality
* In\-depth product page layout
* Has options for reviews on product page
* Social share buttons on product pages
* Login section if the user wanted to register their Mizzie
* About:[https://themes.shopify.com/themes/motion/styles/minimal](https://themes.shopify.com/themes/motion/styles/minimal)
* Preview:[https://themes.shopify.com/themes/motion/styles/minimal/preview](https://themes.shopify.com/themes/motion/styles/minimal/preview)

 

Grid / warm

83 reviews \- 94% positive

Performance rating: 39 / 100

* Design for mums
    * Matches current website
    * Feels a bit Australian and earthy
* Obvious design, buttons and descriptions are where other online\-stores place them.
* About:[https://themes.shopify.com/themes/grid/styles/warm](https://themes.shopify.com/themes/grid/styles/warm)
* Preview:[https://themes.shopify.com/themes/grid/styles/warm/preview](https://themes.shopify.com/themes/grid/styles/warm/preview)

 

Reach / plush

2 reviews \- 100% positive

Performance rating: 59/100

* Childish and fun design
    * Round corners
    * Subtle shadows
    * Subtle decorations
    * Quirky animations
* 'story' pop\-out, which would hold some content from the current "Mizzies World" page
* Breadcrumbs navigation
* About \-[https://themes.shopify.com/themes/reach/styles/plush](https://themes.shopify.com/themes/reach/styles/plush)
* Preview \-[https://themes.shopify.com/themes/reach/styles/plush/preview](https://themes.shopify.com/themes/reach/styles/plush/preview)

 

Split / Jagger

64 reviews \- 100% positive

Performance rating: 42 / 100

* Fun and clean design
* Unique interaction with the header slider
* Animations, although they are subtle
* Sharp corners
* subtle grid borders
* Good for high quality images
* About \-[https://themes.shopify.com/themes/split/styles/jagger](https://themes.shopify.com/themes/split/styles/jagger)
* Preview \-[https://jagger\-theme.myshopify.com/](https://jagger-theme.myshopify.com/)

 

 

 

Created with Microsoft OneNote 2016.
