# Untitled Note

`Project Default`

Initial Nathan

Monday, 20 May 2019

8:45 AM

 

 

1.        What role is Periscope taking?

Phase 1 is just getting the platform in question live in production. There may or may not be some ongoing maintenance – ideally not, for them, but it’s possible that changes / optimisations may be inevitable.

 

2.        What are the expected deliverables from Periscope within the first 6, 12 and 24 months?

I wouldn’t be looking beyond 6 months/Phase 1 right now: priority is to get this functioning, and then we’ll scope with the client what the evolution looks like.

 

3.        Who will be the others stakeholders in Project Default? And what are their roles?

As far as we’re aware it’s really just Jock at this stage. JosephMark will be doing bits & pieces in the background, it might be important for us to understand their expectations of output from us, or if they’ll just build around whatever is being provided. Possibility of some scope creep here if not considered up front.

 

4.        Investors, Project Management, Design, Development, SEO, Marketing, Content, Hosting  etc

Nothing else to consider at this stage. @Jock Fairweather might be the Project Manager. Will need a hosting option, not sure where this is coming from at this stage. Design will be fed to us by someone else. The rest of the items we can probably consider superfluous to the platform/dev brief for the time being.

.

5.        Which devices and platforms will this be running on? Is this a website, app or other?

No app as far as we’ve been briefed so far. The platform selection itself would form part of our recommendation. We already have a pretty fair idea of where it would best be suited based on the brief thus far.

 

6.        What types of media will the website have?

All he’s discussed so far is written/static image content. Video should actually be part of our considerations though, and we’d probably recommend it be included in content/hosting scope.

 

a.        Will webinars be included eventually?

Not part of the brief, or mentioned, so I would say no for Phase 1.

 

b.        Will the videos be hosted on Youtube primarily?

If there are any videos, they would be on YouTube or Vimeo.

 

c.        Will a documents database be required for private document storage?

Not from our end as far as the brief has included so far. I would recommend that the client hosts the content themselves as the project has several iterations planned.

 

7.        In addition to tiered memberships, will there be other items being sold? I.e. merchandise.

No physical merchandise mentioned in the brief. I think we would make allowances for

\-        One \-off content purchases \(single article\)

\-        “View all” subscription

\-        Possibility of a bigger “buy a consultation for $500” but not officially part of the brief. Client to confirm anything beyond the two primary products mentioned above.

 

8.        Will there be a mixture of physical, digital etc. products?

100% Digital at this stage \(unless face to face consultations etc are included as per 7 above\).

 

Created with Microsoft OneNote 2016.
