# Weird person taking photos of my house

2/7/2020

375ybq. Gold coast cc car newest home

possibly future reference?

[https://www.oic.qld.gov.au/guidelines/for\-community\-members/Information\-sheets\-privacy\-principles/camera\-surveillance,\-video,\-and\-audio\-recording\-a\-community\-guide](https://www.oic.qld.gov.au/guidelines/for-community-members/Information-sheets-privacy-principles/camera-surveillance,-video,-and-audio-recording-a-community-guide)

### **Installing cameras**

Regardless of why you are installing surveillance cameras at your home or business1, there are Queensland laws that may apply2.

#### **The Queensland Criminal Code**

Section 227A of the Criminal Code makes it an offence to video record people without their consent in places where they would expect to be private, such as a bedroom, bathroom, or changeroom.

When installing surveillance cameras it is important to assess how they are positioned. Installing your cameras in a way that breaches the Criminal Code may result in prosecution, and generally it is a good idea to minimise their impact on neighbouring properties.

Neighbours, particularly neighbours with young children and/or pools, often feel as if their privacy has been invaded if they are recorded by the cameras. In some cases they may escalate these issues to the Queensland Police Service \(QPS\).
