# Mygov

Mygov username:
**SQ392880**

---

Set my.gov email preferences so that I get an email or a text when something new is in the inbox

My tax return is saying I owe 5,268.77 for Compulsory Higher Education Loan repayment 
Do you know what this is about?

---

**13 28 61**

**All enquiries:****8am\-8pm, Mon\-Fri**
**10am\-4pm Saturday** \(local time\) except national public holidays

_A limited service for myGov/myTax enquiries will operate between_ **_6pm\-8pm_�**�_**on weekdays** and_ **_10am\-4pm_** _**on Saturday**._
_Call 13 28 61 and select option 5._

---

**13 11 42 \- Debt enquiries**

**8am\-8pm, Mon\-Fri**
**10am\-4pm, Sat** \(local time\) except national public holidays

_A limited service will operate between_ **_6pm\-8pm_ _on weekdays�_**�_and�_�**_10am\-4pm on Saturday_**_._
