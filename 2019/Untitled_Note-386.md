# Untitled Note

`JS Industries`

Neto

Monday, 1 April 2019

2:13 PM

| 

|

                                                                                                                                                                                                                                                                                |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
|-|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|No stage by stage, but can custom dev with Zapier

Unlimited products

Neto can support lots of options

Supports tiered prices

 

$3.5m per year and is $2K USD/month with account management

 

under the $3.5m revenue, for $3m it will be $1,849USD/month without the account management

|

|Is there any way to mark stages in a purchase? For example, the customer can be alerted at around 10 different stages of their product being built?

Not out of the box, we may have some custom fields that would work alongside Zapier to do this, but would need to see the Shopify setup first.

 

Is there a limit on the number of products?

 No, all of our plans have unlimited products included.

My client sells custom surfboards, they have around 20 base products, but each can be customised with around 10 different options like height, width, depth, fin layout, glass layout etc. Would Neto be able to make these notes somehow? How would this be presented on the Shopify end?

This depends on how the integration would work. We would need to see what the front end looks like, because, from our knowledge, Shopify has a limit on the number of product options. We are assuming you would be adding custom functionality to this? With a Neto front\-end, we can do this and have some examples for you.

 

My client needs tiered prices for wholesale. As well as percentage discounts, and specific shipping discounts. Can Neto support this?

Yes, you can have tiered pricing for wholesale customers and you can have customer\-specific discounts/shipping discounts. 

 

Is there any extra onboarding/development which would require an additional cost?

We provide onboarding as part of the subscription fee, so it does not cost any extra. In terms of development, what are you referring to? If you are to fall into our XL/Enterprise plan \(see below for more\) your client will have a higher level of support around any potential development needed.

 

Finally, am I able to get a rough ballpark for pricing? They would be making around $3m / year in sales.

This is on the borderline of our Enterprise \(XL\) plan size which starts at $3.5m per year and is $2K USD/month with account management. If it is under the $3.5m revenue, for $3m it will be $1,849USD/month without the account management. Happy to discuss this with you in more detail over the phone, but would make the most sense to go with the XL package for the small increase of about $150 per month.

 

In the interest of time, I hope this covers what you need to know for tomorrow, but look forward to hearing the outcome and putting some time in the diary next week to help further.

|
| 

|

                                                                                                                                                                                                                                                                                |

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |

 

Created with Microsoft OneNote 2016.
