# Fallout 4 builds

Other peoples ideas:

Sneak sniper build:
strength 1
perception 4
endurance 1
charisma 1
intelligence 3
agility 10
luck 7
go with idiot savant first, then lockpicking, then everything sneak related, then gun nut, then rifle damage.

Sneak melee \(OP as heck\)
strength 4 \(you basically one shot everything anyways...\)
perception 4
endurance 3 \(shjt happens\)
charisma 1
intelligence 1
agility 9
luck 5 \(why not?\)
idiot savant \> lockpicking \> sneak related skills / blitz \> melee weapon damage skills
by the time \(if at all\) enemies aren't always killed before the sneak damage wears off, you can probably just invest 5 more into strength and get the rooted skill \(works with blitz\).

---

**Note:**
Get the bobbleheads after you get the specials to 10. The bobbleheads will put it up to 11

START WITH 28 \(but can assign 21 at start\)
3 are easy to gain

**My Build**

strength 1
Perception 4
Endurance 1
Charisma 1
Intelligence \- 6
Agility \- 3 \(add 3 asap\)
luck 5

As soon as you reach Sanctuary after leaving Vault 111, visit the nursery in your old home. There’s a book called You’re SPECIAL on a cabinet here. Read it to gain one extra SPECIAL point, and put it on Agility to bring your total to seven.

**My / Infiltrator build**
[https://www.vg247.com/2015/11/24/fallout\-4\-build\-guide\-infilitrator/](https://www.vg247.com/2015/11/24/fallout-4-build-guide-infilitrator/)

|st|3|
|--|-|
|pe|5|
|en|1|
|ch|1|
|in|3|
|ag|6|
|lu|9|

1. Join Railroad faction
2. Get deliverer
3. Check in with tinker tom for mod

Perks
|Locksmith       |1,7,18,41   |
|----------------|------------|
|Sneak           |1,5,12,23   |
|Ninja           |1,16,33     |
|Action Boy      |1,18        |
|Gunslinger      |1,7,15,27,42|
|Bloody Mess     |1,9,31,47   |
|Local Leader    |1,14        |
|Cap Collector   |1,20,41     |
|Armorer         |1,13,25,39  |
|Gun Nut         |1,13,25,39  |
|Scrapper        |1,23        |
|Science         |1,17,28,41  |
|Better Criticals|1,15,40     |
|Gun\-Fu         |1,26,50     |

|1 |\-              |
|--|----------------|
|2 |Locksmith       |
|3 |Ninja           |
|4 |Gunslinger      |
|5 |Sneak           |
|6 |Sneak 2         |
|7 |Gunslinger 2    |
|8 |Locksmith 2     |
|9 |Action Boy      |
|10|Bloody Mess     |
|11|Bloody Mess 2   |
|12|Sneak 3         |
|13|Local Leader    |
|14|Cap Collector   |
|15|Gunslinger 3    |
|16|Ninja 2         |
|17|Gun Nut         |
|18|Action Boy 2    |
|19|Locksmith 3     |
|20|Scrapper        |
|21|Science         |
|22|Better Criticals|
|23|Sneak 4         |
|24|Local Leader 2  |
|25|Armorer         |
|26|Armorer 2       |
|27|Gunslinger 4    |
|28|

                |
|29|

                |
|30|

                |
|31|Bloody Mess 3   |
|32|

                |
|33|Ninja 3         |
|34|

                |
|35|

                |
|36|

                |
|37|

                |
|38|

                |
|39|

                |
|40|

                |
|41|Locksmith 4     |
|42|Gunslinger 5    |
|43|

                |
|44|

                |
|45|

                |
|46|

                |
|47|Bloody Mess 4   |
|48|

                |
|49|

                |

---

Level up faster:
**Repeatable mission:**
One method for earning unlimited amounts of XP in a fairly short time is through a Radiant quest from the Brotherhood of Steel. After completing a few Brotherhood faction quests, players will eventually gain access to the faction’s airship, Prydwen. Once upon the airship, head to the main deck and speak to Proctor Quinlan. He offers a Radiant quest called Learning Curve, which can be played repeatedly for unlimited amounts of XP. Agree to help out on patrol to activate the quest.

Prepare for this quest by equipping a long\-range, high power sniper rifle, preferably with a scope. You may also want to lower the difficulty setting to Easy in order to expedite this process. Although this quest tasks you with protecting a fellow Brotherhood ally, you are basically going to just repeatedly kill this person instead
