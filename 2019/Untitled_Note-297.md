# Untitled Note

`Process`

Example meeting follow up

Thursday, 12 September 2019

12:26 PM

Hello Myriam, Jasmine & Sara,

 

Great to connect on Tuesday and talk about the next steps for Bricksmegastore.com

 

Sara, thanks for sharing the documentation from Lego and the exisiting assets Bricksmegastore.com have had produced.

 

I wanted to summarise the key points we discussed and provide some detail around timings.

Bricksmegastore.com brand \(we appreciate you have an existing asset here \- the red brick\) in addition if there is no visual text representation of the brand it's something you may wish to consider creating. We can flow some ideas through here.

Holding page update \- We'd like to present some concepts to you early next week on this and subject to changes / approval this could be updated and live next week.

Target go live Wednesday 18th September

It would be great if you could let us know some availability on Monday / Tuesday next week for maximum 1 hour to review designs.

Content website design & build \- thank\-you for sharing the website copy \- we're currently turning this in to an updated plan / brief which we can speak about on a call early next week also. We will begin designs on this early next week and aim to provide some concepts around Friday 20th / Monday 23rd, again we will need some time to present these to you \(probably in person\)

eCommerce website \- A target go live for this site is February 2020. Over the next couple of weeks I can elaborate more on the steps towards gathering requirements and having this platform designed and developed.

Lastly, It was raised how quoting and charging would work to ensure that the Lego team has transparency & control \- As I mentioned I'll connect with Justin / Richard to get their view on a process and we can agree it from there.

 

Let me know if there's anything Ive missed or if you have any questions.

 

Thanks,

 

Martin

 

Created with Microsoft OneNote 2016.
