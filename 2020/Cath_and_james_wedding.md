# Cath and james wedding

1. Cath always been a close friend while growing up. Shared interest in rowing, walking and trying to be healthy.
2. Encouraged me to be respectful of others and try to find the light in situations.
    1. Traits all of us have
3. Had been an ear or a shoulder to me many times.
4. Kept connections tight despite geography
5. James is a new branch in the family tree
6. A man I'm proud to call a brother.
7. gives us comfort Cath is taken care of
8. Respects Cath, 
9. brings out her best traits,
    * Health/fitness
    * Positive attitude
    * Adventuring and exploring
