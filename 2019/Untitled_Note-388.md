# Untitled Note

`JS Industries`

3p Inventory management

Monday, 1 April 2019

2:11 PM

3p questions

1. Support Stage by Stage updates
    1. Update order to give info on build status \- which can then be sent to the customer?
    2. Around 30 different options.
2. Limits on:
    1. Number of Stockists?
    2. Locations and Warehouses?
3. Products with about 50 \- 100 variants?
4. Multicurrency
5. Where is support and what hours?
6. Onboarding fees and custom dev installation?
7. Include information on shipping/freight costs
8. Integrate with Shopify POS?
9. Prices

 

Created with Microsoft OneNote 2016.
