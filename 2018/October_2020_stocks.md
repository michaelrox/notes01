# October 2020 stocks

Sep 22:

\-\- Hold off for a few days \- prices all going sideways and market expecting to go down.

Watching:

**KSS**

\- Data storage in satelites

\- Currently at 0.30

Reddit hype: 3/10

**KTG**

\- Super fast welding hardware

\- Currently at 0.26

Reddit hype: 3/10

**BUD**

\- Commercial IoT gadgets \(lights, bacteria killing lights, carbon monitor, capacity...\)

\- Looks like a fair price atm, Morningstar estimate it should be worth double.

\- Sold in Apple in HK and Singapore

\- Bacteria lights are approved in USA

\- Reddit hype: 5/10

**VR1**

\- Commercial Virtual Reality, partnered with JM, Dell, Volvo and a few other big ones.

\- Currently a bit hyped due to a positive yearly summary \- Morningstar says it's valued at the current price \($0.08\)

\- Reddit hype: 7/10
