# Reddit Index

http://www.bogleheads.org/wiki/Lazy\_portfolios

\-\-\-\-
Visit Vanguard, they are the best priced. 

Buying an ETF is identical to buying any other stock, you just need a broker. CmcMarkets is the cheapest Australian broker. 

Buying into a fund just required contacting Vanguard and hanving the prerequisite $5000 to start. 

\-\-\-\-
Well, an index is just buying a basket of different shares. There is no real process to buying an ASX200 index ETF, just purchasing the share gives you ownership of those 200 companies. Doing it yourself would involve individually buying each of those 200 companies, which would be expensive and inaccurate. 

However, in your case, an index fund would be the better idea. You need $5000 to start investing, but then you can add money to it whenever you want without extra charge. This could be as simple as setting up an automatic bpay to your fund account each week. 

It's a Good idea to diversify outside of Australian equity. I do 30% for each of Australia, US and Europe. 
