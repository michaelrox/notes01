# 20/10/2017

|Tag                                                                         |Title              |Time \(hours\)|
|----------------------------------------------------------------------------|-------------------|--------------|
|[LMSCORE\-829](http://jira.mediasphere.com.au:8080/browse/LMSCORE-829)      |Button consistency |5             |
|[POWERHOUSE\-110](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-110)|Website 2 step form|1             |

|Tag                                                                         |Title                        |Comment                                                                          |Time \(hours\)|Pr|
|----------------------------------------------------------------------------|-----------------------------|---------------------------------------------------------------------------------|--------------|--|
|[LMSCORE\-829](http://jira.mediasphere.com.au:8080/browse/LMSCORE-829)      |Button consistency           |asap. started wednesday and interuppted other work.                              |5             |1 |
|[POWERHOUSE\-110](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-110)|Website 2 step form          |quoted 1 hour, due today                                                         |1             |2 |
|[EALDHUBMAS\-69](http://jira.mediasphere.com.au:8080/browse/EALDHUBMAS-69)  |Accessibility Review Document|hard to quote, was in the middle of a bad place when interrupted for lmscore\-829|              |  |

Core

|Tag                                                                   |Title                               |Comment|Time|Priority|
|----------------------------------------------------------------------|------------------------------------|-------|----|--------|
|[LMSCORE\-806](http://jira.mediasphere.com.au:8080/browse/LMSCORE-806)|my misc bugs                        |       |    |        |
|[LMSCORE\-807](http://jira.mediasphere.com.au:8080/browse/LMSCORE-807)|certificat manager idea             |       |    |        |
|[LMSCORE\-797](http://jira.mediasphere.com.au:8080/browse/LMSCORE-797)|Text on Capabilities and Risks Graph|       |    |        |
|[LMSCORE\-658](http://jira.mediasphere.com.au:8080/browse/LMSCORE-658)|Responsive tables                   |       |    |        |
|                                                                      |                                    |       |    |        |

Powerhouse website

|task                                                                        |Title                             |Comment                                                                                                                                                                |Priority|time\(hr\)           |
|----------------------------------------------------------------------------|----------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------|---------------------|
|[POWERHOUSE\-100](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-100)|Course Library                    |Rebuild Course library pages                                                                                                                                           |4       |8                    |
|[POWERHOUSE\-94](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-94)  |Remaining changes and fixes       |\- Special characters for posts

\- CK Editor fix

\- update contact forms

\- coloured italics on Course pages

\- remove class on blog items

Done
Assortment of bugs. Mine are:|        |                     |
|[POWERHOUSE\-99](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-99)  |Homepage \> Redesign              |

Done
design and dev home redesign                                                                                                                                       |1       |8                    |
|[POWERHOUSE\-102](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-102)|Key features link on products     |draft01 done, sent to Chus for feedback                                                                                                                                |1       |5                    |
|[POWERHOUSE\-98](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-98)  |Product pages                     |Need feedback from Chus or Keyleigh \- if none given then I'll do my best \(again.\)                                                                                   |2       |33 image \- 36 hours?|
|[POWERHOUSE\-103](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-103)|Social media buttons on blog posts|                                                                                                                                                                       |3       |1                    |

Todays time has gone:

8:30 \- 9:30 \- organising tasks

9:30 \- 10\- powerhouse\-102

10 \- 11:30 \- Powerhouse\-103

11:30 \- 1:30 \- Powerhouse\-98

1:30 \- 2:30 \- review

2:30 \- 3:30 \- [POWERHOUSE\-102](http://jira.mediasphere.com.au:8080/browse/POWERHOUSE-102)

3:30 \- 4:30 \- Powerhouse\-100

Today:

Powerhouse\-102

Powerhouse\-100
