# Grange email

The Grange website will be running late

 

The original three weeks development time quote was under the assumption I would have no other work for those three weeks. This would equal 120 hours assuming \(8 hour days, 5 days a week, 3 weeks.\) My timesheet shows that I have only had 53 hours to work on the project to date due to other tasks being placed under my responsibility \(mostly support issues where I am the only person with the knowledge to help.\)

 

On top of this I have unfortunately been sick for two of the days.

 

I am capable of pushing out something that can be sent to The Grange for assessment by May the 29th.

 

We could still potentially go live on the 15th of June but the site will need to skip a round or two of QA testing. The result will obviously be a lower quality website.

 

Again this estimate assumes nothing urgent comes up and I’m back to full health by tomorrow.

Different email:

Finally, I still need the following:

Videos URLs for course tour, I would prefer if they were already uploaded to YouTube or Vimeo.

Course tour images across east and west are inconsistent.

East course images show different course distances to the website.

Online store \- I will need some items
