# Bonnie Doon updates

* QA for the current dev site \(Redesign 1\)
* include some images or annotated screenshots
    * They want the look and feel to match 
        [http://www.metropolitangolf.com.au/cms/](http://www.metropolitangolf.com.au/cms/)
        [http://www.concordgolfclub.com.au/cms/](http://www.concordgolfclub.com.au/cms/)
        [http://www.thelakesgolfclub.com.au/cms/](http://www.thelakesgolfclub.com.au/cms/)
    * Also want to inner\-page suggestions

fix bankstown nav

emailed to Colleen on 29/03/2017

Hey,

I have mocked up some concepts of a new home page to include similar features as the Concord website. I have also thought about ways to make it unique in it's own way. This is mainly visible in the header/navigation at the top.

Please let me know what you think of the following concepts, the only variance is the header/navigation bar at the top:

Option 1:

me kn http://miclubrd1.miclub.com.au/cms/wp\-content/uploads/2017/03/home1.jpg

Option 2:

http://miclubrd1.miclub.com.au/cms/wp\-content/uploads/2017/03/home2.jpg

Option 3:

http://miclubrd1.miclub.com.au/cms/wp\-content/uploads/2017/03/home3.jpg

When the user scrolls, a similar transition will happen as on the current website \(http://miclubrd1.miclub.com.au/\) where the logo will be shrunk and the navigation bar will take up less of the users screen.

I could also do a blend of these for example:

\- Option 2 buttons but with a bigger logo and no white banner

\- Option 1 just for the home page and option 3 navigation on all other pages. And then Option 2 buttons when the user scrolls down.

If none of these are ticking the correct boxes, at least this can encourage some feedback and push me in the right direction for what Barwon Heads likes and doesn't like.

 
