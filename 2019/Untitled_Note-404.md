# Untitled Note

`JS Industries`

Current site

Tuesday, 12 March 2019

3:33 PM

| 

|

                                              |

|

                                   |
|-|----------------------------------------------||-----------------------------------|
| 

|Stack:

Magento

Cavewire

Google Analytics

New Relic

|

|3D tool

Reached out to:

Expivi

Zakeke

 

|
| 

|

                                              |

|                                   |

Hi,

I have a client that is looking to migrate to Shopify, but they currently have a custom built tool that builds a custom surf board, and also provides a 3d model for the customer while designing. This is built in to the eCommerce CMS they currently use and want to move away from.

 

An example is here: [https://www.jsindustries.com/surfboards/the\-vault/blak\-box\-2\-round\-tail\-hyfi](https://www.jsindustries.com/surfboards/the-vault/blak-box-2-round-tail-hyfi) if you click ‘custom order’

 

![0dcbd961-ce43-4d4f-9846-9ccafb26c7da.jpe](image/0dcbd961-ce43-4d4f-9846-9ccafb26c7da.jpe)

 

 

I’m wondering if you would be able to provide similar functionality and integrate with Shopify? If so then could  you please provide a quote?

 

 

 

![d902a075-89fe-4fb0-80f0-c66c3faf2d0f.gif](image/d902a075-89fe-4fb0-80f0-c66c3faf2d0f.gif)

 

Michael Hiley

eCommerce Project Manager

 

E. [michael@thisisperiscope.com](mailto:michael@thisisperiscope.com)

Ph. \+614 07 505 328

W. [thisisperiscope.com](http://thisisperiscope.com/)

 

Created with Microsoft OneNote 2016.
