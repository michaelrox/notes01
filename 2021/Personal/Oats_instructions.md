# Oats instructions

Oats

1/3 cup almond milk

1/3 cup water

1/3 cup rolled oats

1 tsp cinnamon

Combine oats, milk, water, and cinnamon in a small saucepan. Bring to a boil, then reduce heat to low.

Simmer uncovered for 3 to 5 minutes until thickened, stirring occasionally. Remove from heat and let cool slightly.

Top with sliced banana \(maybe just half\) and some honey
