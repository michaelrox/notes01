---
created: 2021-09-10
---

# Create Trello card in WIP

Create Trello card in WIP

create branch. branch names use underscores
e.g. patch_<trello id>_logo_fix

Go to github website
Open pull request
Add someone in to the "Reviewers"

Once reviewed - click "Merge Branch"
Then "delete branch"

Go back to repo home
Go to "release" on right hand column
Make the tag the version 1 higher than the previous, like 1.0.5

-- usually I should let GT know that there is an extra release, "can someone please update the latest when they do the next deploy"

> composer.json may need to update if the version goes above 2. something

```cd ~/Containers/Fool-AU/public/
git pull - very important - this makes sure we don't overwrite recent changes
lando composer require fool/freesite-2020-theme:^1.0.0
git status
git -a
git add composer.lock
git commit -m "lock"
```


go to jenkins
go in to "Build Fool AU" - wait for spinner
Deploy Fool AU staging - wait for spinner
* While spinning, Jenkins is testing ecaps and hops etc.
* While spinning, I should test ecap and hops as well.

back in Trello - move card in to Dev: Review and Revision
