# Untitled Note

`Mr Poolman`

5/03/19

Tuesday, 5 March 2019

8:28 AM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |

|
|-|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||
| 

|I like the layout. I want to see refinements on the left be:
Shop By Category
Shop By Type
Filter By Brand
Filter By Price
    * Category being sub\-cats
    * Type bring sub\-sub\-cats
    * Brand \(can we have the top 6 brands to display their logo rather than the hyperlinked text?\). Can we also customise these brands so Water TechniX is always displayed first?
    * Price \(can we add a $1000\-$2000 price bracket?\)
    * Can the refinement box turn blue and "tick" when selected
From \<[https://app.azendoo.com/\#workspaces/5acfcb80f7339b1c0600002b/conversations](https://app.azendoo.com/#workspaces/5acfcb80f7339b1c0600002b/conversations)\>
    1. Category and Type centred, optional copy below, breadcrumb top left, drop down right. Selected refinements can sit on top of the refinements.
    2. Do we have to have white space on either side? Can we fill this with 5 products per row as opposed to 4 on desktop?
From \<[https://app.azendoo.com/\#workspaces/5acfcb80f7339b1c0600002b/conversations](https://app.azendoo.com/#workspaces/5acfcb80f7339b1c0600002b/conversations)\>
\\

|

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            |

|
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|Refinements:

Not sure how to get these to work exactly as you asked. We can show the real vendor as a checkbox, and we can rename the section to 'Brand'

 

Brands being logo \- not likely possible with the IS\+ layout

 

Price \- this is automatically calculated, when there are products in that price range then those checkboxes will show

 

Checkbox with 'tick' \- probably

 

\-\-\-

1. Sure this should be possible, I'll try to organise a quote for dev.
2. Don't have to have white space, will add these changes to the above.

\-\-\-

 

I'll wait for LimeSpot to reply to the email you sent, for now I can say:

1. Not sure, I don't feel confident though.
2. Will be custom dev
3. Will be custom dev
4. Yes, I'm pretty sure I saw that in the settings
5. I don't see the harm in testing the quick add to cart. We just need to make sure it is styled different to the quick view button, so that users don't just click the button and assume a different response.

 

 

 

 

 

||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ||
| 

|Things that need to be tweaked so far:

1. Shows on Cart page though not on AJAX cart, can we make AJAX cart it's own section to choose boxes?
2. Carousel to display exactly as we have it currently on live store
3. How can we edit position of boxes on site
4. I really like this attached up sell on the cart page, can we make this 3 items rather than one?
5. Should we keep the "add to cart" button on mouse\-over? I like this as it's frictionless and I feel upsells should have a one\-click add to cart option. We currently have "Quick View" on mouse\-over as in the near future products will be having variants that need to be chosen before adding to cart. Thoughts?

 

From \<[https://app.azendoo.com/\#workspaces/5acfcb80f7339b1c0600002b/conversations/5c7cab56a8e6044f1300000a](https://app.azendoo.com/#workspaces/5acfcb80f7339b1c0600002b/conversations/5c7cab56a8e6044f1300000a)\>

                                                                                                                                                                                                       |

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            ||

 

Created with Microsoft OneNote 2016.
