# Yarra Yarra

initial questions

What is the task?

Email on 7/03/17 makes it look like a Members Home page update.

\- last contact I had about this Members Home page was 16/01/17 where Colleen mentioned she would get Tom to do what he could.

\- The quoted work I responded to was sent by me on 8/12/16

Email on 2/3/17 makes it look like a pretty straight forward Custom website design. When I sent the reply saying I required no extra information, the larger email by Peter wasn't included so I didn't know this was related to that.

This email \- send to me on 7/3/17 includes the original confusing request by Peter Vlahandreas  which was sent on 17/11/16 to Chris. If that is the case then I have no idea what my task would be. Task 1 sounds like a new members home page which would be similar to Pennant Hills gc with lots of different news feeds. Task 2 is very confusing, is it a redesign, separate website, or something else? 

---

Email from Peter 17/11/16

**Task 1�**�

* new Members Home page. The "App Style" boxes are accordion sections? not sure what he means by app style.

**Task 2 \- is this a custom website design? is it a redesign? is it seperate to the current website?**

* entirely new website \- non\-golf club...but priority 1 is Leaderboards? 
    * example [http://www.rydercup.com/scoring](http://www.rydercup.com/scoring) to set up leaderboards like this will need to be made by the backend Miclub team.  Get quote from them.
    * example \- pga.org.au \- I couldn't find where the leaderboards are.
* e\-cal 
    * I mentioned in my email on 8/12/16 that the e\-cal interaction won't be fixed by any of his suggestions.
    * I think the fixtures/bookings section at the moment already has a link to export to calendar... Check with Tom
* Hospitality / event bookings \- again this is back\-end that would need to make changes to this. Get quote from them.
* Better use of galleries? as in better use of photos? just tell me what they want
* Distinct Member & Visitor pages. I could have sworn they already experimented with this and it proved to be annoying.
* Superior Mobile site.  \- again this is back\-end that would need to make changes to this. Get quote from them.
* flipboard.com ... This website that is worth $70m ... but seriously, even if there is a plugin which can replicate that sites functionality, in order to truthfully serve the users we should trust them to use an external site like that instead of piling more useless shit on this website.
* His point about only adding the most important things has been completely hypocritical. Without a doubt the original designer \(Paula?\) had to fight the exact same battle.

**Timeline estimate:**

* 600 hours? 
* Usual design is 60 hours. Typical build is around 140 hours. This seems like it will take around 3 times the usual time.

It is worth asking how they expect all of this to impact the club. Will it have any positive impact? Is there anything to gain? The clubs money and our time is better spent on refreshing what they currently have, removing dead weight like pages which don't generate money or interest. Our data shows that 99% of users are going straight to the members home and bookings page and most of those users are going straight there by a saved bookmark.

The club needs to first establish why they want to update the website. Figure out how updating it will get them a monatery return. For example leading more visitors to look at the functions pages or membership sign\-up pages. If they honestly only want to improve the members user experience then it is best to have these things designed by professional designers. The first step would be to question a large random sample of members and ask them for opinions. 

The proposed solution will just confuse most users \(not many users understand how to use accordion news sections with scrolling capabilities, that is very confusing even for someone like me.\) For example look at flipboard.com and you will see how simple it is, even if you subscribe to over 20 categories and each with over 50 rss feeds, you will still only ever see 1 news stream at a time.
