# Perth Plan

stick to shares now, build portfolio for the year

travel, maybe dip in to shares to fund travel

come back to aus, have baby, start saving by putting straight in to super first....

write budget

Aim to save x amount in the year \- 20,000 to 30,000?

\- put 10% of savings in to super

\- 90% of savings in to shares

While in Perth:

Every $10,000 \(3ish months\)

$25,000 invested in shares by November 2016 \(minimum\)

|VGB \- Aus Gov Bonds                  |10%|
|--------------------------------------|---|
|VAS \- Aus Shares                     |20%|
|VGS \- International \(excluding Aus\)|20%|
|Misc.                                 |50%|

**5% \(min\), 7% \(Buffets magic number\)\- 9% \(max\)**

Compound interest calculator:

$25,000 \- no further deposit, compounded annually at 5% \(assuming this is the lowest possible interest earned from shares\):

|Time                   |5%      |7%      |9%      |
|-----------------------|--------|--------|--------|
|5 Years Total Interest |$6,907  |$10,064 |$13,466 |
|5 Years Total          |$31,907 |$35,064 |$38,466 |
|10 Years Total Interest|$15,722 |$24,179 |$34,174 |
|10 Years Total         |$40,722 |$49,179 |$59,184 |
|40 Years Total Interest|$151,000|$349,361|$760,236|
|40 Years Total         |$176,000|$374,361|$785,236|

\-\-\-\-\-\-\-\-\-\-\-\-

VGS \- $20,000

VHY \- $4,000

Maybe later:

|VGB \- Aus Gov Bonds                  |10% |
|--------------------------------------|----|
|VAS \- Aus Shares                     |30% |
|VGS \- International \(excluding Aus\)|30% |
|VAP \- Property                       |10% |
|VHY \- High Yield                     |10% |
|Misc.                                 |save|
