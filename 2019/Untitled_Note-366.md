# Untitled Note

`FCS`

Initial run down

Wednesday, 24 April 2019

3:04 PM

Parent company is SHI \- Surf Hardware International.
Brother companies:
    * [https://www.surffcs.eu](https://www.surffcs.eu/)
    * [https://www.softechsoftboards.com.au](https://www.softechsoftboards.com.au/)
    * [https://www.kanulock.com.au](https://www.kanulock.com.au/pages/keyvault)
    * [https://www.gorillasurf.com](https://www.gorillasurf.com/products)
    * [https://www.hydrosportz.com](https://www.hydrosportz.com/buy-online)
    1. Should they move HydroSportz and GorillaSurf to Shopify?
        1. Squarespace is equal to Shopify for SEO
        2. Developers would need to be familiar with 2 platforms meaning they can't spend as much time specialising and staying on top of new features.
        3. The dev's obviously knew what they were doing, both stores currently score really high for performance.
        4. Can't make online purchases for Gorilla atm.
    2. Performance audit of FCS
        1. Quick look \- Google thinks it's bloody fast. Scores 94/100 on Google Lighthouse for mobile
            1. Reality is that it's great from a robot point of view, but for humans there is still work to be done. \(outlined in audit table.\)
        2. Gorilla Grip scores 100/100...
        3. SEO is technically high on both.
            1. Gorilla having a splash page is poor UX \- outdated by about 10 years \- splash pages have proven to only serve as a possible place to bounce users.
            2. Blog articles could have more obvious CTAs and links throughout \(outlined in audit table.\)
            3. Blog should include author and location if applicable to help with search engine personalised results.
        4. Data acquisition:
            1. The current newsletter sign\-up form takes the user away from the FCS website \- creating an exit to the sales funnel \- also shows Mailchimp favicon. The Enquiry form is simple and tidy, but hidden in an iframe on a hard to find page. \(outlined in audit table.\)
            2. Members area is basic, and doesn't feel personalised at all.
        5. Social integration
            1. No option to share products or purchase
            2. Opportunity for shoppable instagram\-like feed to keep customers shopping experience flowing. \(show related products in product description page etc.\)
        6. Support page is sloppy \- just thrown in the Desk FAQ in an iframe \(referred to in audit table.\)
            1. Desk will stop functioning very soon and force customers on to service cloud
        7. Purchase funnel is messy
            1. Primary CTA on home is hard to identify \- it is the entire slide. This is poor for SEO and usability.
            2. Next page the youtube video takes up the whole slide and the user may not know to scroll. The primary CTA should be "Shop Now" and should be on top of the video. Also the Video needs to be clicked to play, which could then lead the user outside of the website to youtube.
            3. Product PDP is accessible in 2 clicks, and add to cart is 3rd. On mobile the user has to scroll to get to the add to cart button.
        8. Cart page has a messy layout but some nice features that are leading trends. \(instant payment and shipping calculator.\)

 

Created with Microsoft OneNote 2016.
