# Template

**Dynamic Stretches \(5\-10 reps\)**
Wall ExtensionsBand Dislocates

Cat/Camel Bends

Scapular Shrugs

Full Body Circles

Front and Side Leg Swings

Wrist Mobility

**Bodyline Work \(10 \- 60sec\)**

Plank

Side Plank

Reverse Plank

Hollow Hold

Arch

**_Warm\-up activity \(optional\)_**

Squat Jumps

Burpees

**Handstand Progression \(5 \- 10min\) \(pick 1\)**

Wall Plank

Stomach\-to\-wall Handstand

Freestanding

**Support Progression**

Levers \(freeze\) 

Crow

**Pullups A1 \(3x5\-8\)**

Pull Up

Knees in front

L\-sit Pull up

Pull Over

**Dips�**�**A2 \(3x5\-8\)**

Parallel Bar Dips

Knees in front Dips

L\-Sit Dips

**Squats B1**

Bodyweight Squat

Deep Step\-Up

**L\-sits B2�**�L\-sit Progression \(3x10\-30s\)

Foot\-Supported L\-sit

One\-leg Foot supported

Tuck L\-sit

One\-leg L\-sit

L\-sit

**Pushups C1** Pushups \(3x5\-8\)

Pushup

Diamond

Decline

Decline elbows in

One\-Arm Wall

Incline One\-arm

One Arm

Decline One Arm

**Plank�**�**\(5 \- 10min\)**

Kneeling

plank

side

decline
