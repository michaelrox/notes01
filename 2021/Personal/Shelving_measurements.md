# Shelving measurements

Nursery

Closet:

2m wide. 1m halfway

1.7m tall

42cm deep

L

92cm behind Door

2.58m wall of door

3.5m blank wall

Study nook:

1.8m wall next to window

60cm. Desk deep

Study room:

1m X 1.29m \- closet

80cm. Between desk and open closet door

3.5m blank wall

1.79m between corner desk and door wall

1.98m between door and blank wall

51cm X 41cm \- wedding canvas

1.7m \- buffet

Ideal big art size for upstairs and sides next to aircon:

80cmx100cm
