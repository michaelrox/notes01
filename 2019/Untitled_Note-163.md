# Untitled Note

`Tasks`

Pool Covers emails...

Monday, 17 June 2019

10:21 AM

Paul has written back saying that Shopify doesn't offer or do any development work as part of their service.

He has mentioned another 2 different ways to tackle the problem. Both of these would take equal or longer than things already tried.

 

The feedback isn't really feedback. The facts still remain \- we want this to function as easily for the user as possible.

 

I can get this to work at a buggy level, but I would stick to my original plan as it is better UX.

Having long drop down lists to select numbers is frustrating, particularly on mobile.

My original quote was 8\+ hours and I still believe this would be the case. I don't see any of the feedback provided to far reducing that time.

 

 

\-\-\-

 

Hey,

I agree that the current best solution on the 250b is the best solution on offer.

I was waiting for some more hands on feedback as Tom made it sound like yourself and Jason had offered this earlier.

 

We would rather use number input boxes instead of dropdown menus. Drop down menus are more friction for the user. Adding the min\-max options for number input would probably take just as long as manually creating the dropdown menu. Plus this wouldn't be easily re\-usable code as different products have different min\-max's.

 

Creating the 7200SKUs is another large task, this isn't the best option if we are trying to save time. Likewise creating the logic to add to cart would take time and require further custom development, plus the same development that we are currently facing with the current best solution. Plus we are ideally looking for a solution that is re\-usable for other similar products in the future.

 

Thank you for letting us know that Shopify doesn't offer services in this area, it's good to know that we can focus on solutions elsewhere.

 

Thanks

 

 

Created with Microsoft OneNote 2016.
