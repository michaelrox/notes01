# Untitled Note

`JS Industries`

Manufacturing process

Wednesday, 8 May 2019

1:34 PM

I'm acting as a consultant for my client JS Industries \- [https://jsindustries.com](https://jsindustries.com)
They primarily sell custom surfboards.
They average around $60,000 sales / month
They average around 200 custom surfboard builds / month.
The process has up to 20 steps sometimes.
The process can also sometimes involve sending a digital file to a 3D shaper.
The solution I'm looking for will need to automatically update a surfboards order status every time it goes between each stage.
The service must integrate with Shopify. Ideally this would integrate with CIN7 unless you are able to provide all of the features CIN7 can \(including wholesale, multicurrency, working with 2 xero accounts.\)
In touch with:
    * Zebra
    * Scannit
    * Coridian
    * keystonecorp

 

Created with Microsoft OneNote 2016.
