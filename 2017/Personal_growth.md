# Personal growth

2018: Lead website design.

If Nik & Alex create more brands, it will be worth getting a javascript heavy developer.

A junior who is dedicated to just web would be a lot more useful.

2023 

\- company now has over 200 employees. 

\- I'll be head of creative/design. With Marketing and Design as seperate departments.

---

My work:

Code: front end. e.g. look and feel. 

Design: Create marketing collateral, primarily interactive design. Also creative illustrations

Quality assurance \(e.g. ensure brand consistency and quality across all media forms\)

Not me:

Analytics \- the analysing side.

Sean can do EDM templates
