# 13th Beach Verushka feedback

Layout/Home

* Foote
* Left\-hand menu – not keen on the left\-hand border on active pages. Add in whatever styling you do for the navigation hovers/active items to keep the design consistent.

Golf \-[http://miclubrd8.miclub.com.au/cms/golf/](http://miclubrd8.miclub.com.au/cms/golf/)

* Yeah, the images should be the same height

Course Tour \-[http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* The heading for “36 Spectacular… etc” is smaller than the “Creek Course” and “Beach Course” sections… kinda not sold on this, especially as I can see them both on the page at the same time. Increase the space above the split section too as it kinda all runs into eachother.

Creek Course \-[http://miclubrd8.miclub.com.au/cms/golf/course\-tour/creek\-course/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/creek-course/)

* I like this layout. I’ll probably steal it at some point

Course Tour – Single Hole pages \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1\-2/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1-2/%29)

* More space between the video and the birdseye image would be nice. Also, the video has a lot of black space around it – not much we can do. Maybe make video taller?

Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)

* Reduce table cell padding. Keep the \<hr\>’s or borders under sections consistent too \(the one under the table isn’t as thick\)

Golf Shop \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-shop/](http://miclubrd8.miclub.com.au/cms/golf/golf-shop/)

* I like the heavy heading paragraph with the regular text. Maybe just add some extra space below the heavy text though.

Accommodation \-[http://miclubrd8.miclub.com.au/cms/accomodation/](http://miclubrd8.miclub.com.au/cms/accomodation/)

* More space between image and text would look great. Also, is onsite two words?

---

New Email:

The rest of the design shiz… and then  some\!The rest of the design shiz… and then  some\!

* ~~When you hover over the nav items, some aren’t centred as in there’s not equal space around the word behind the blue background.When you hover over the nav items, some aren’t centred as in there’s not equal space around the word behind the blue background.~~
* The “History of…” text on the home page has turned black.The “History of…” text on the home page has turned black.
* Home page \- “Sign up to our newsletter” looks oddHome page \- “Sign up to our newsletter” looks odd

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* The “Creek Course” and “Beach Course” copy has turned blue/got larger.The “Creek Course” and “Beach Course” copy has turned blue/got larger.

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

* The links aren’t opening in a new window \(this content looks final, so just pointing it outThe links aren’t opening in a new window \(this content looks final, so just pointing it out

Contact \- [http://miclubrd8.miclub.com.au/cms/contact\-us/](http://miclubrd8.miclub.com.au/cms/contact-us/)Contact \- [http://miclubrd8.miclub.com.au/cms/contact\-us/](http://miclubrd8.miclub.com.au/cms/contact-us/)

* Maybe remove the blue background from the “Sign up to our newsletter” bit on the form? It looks a lil odd.Maybe remove the blue background from the “Sign up to our newsletter” bit on the form? It looks a lil odd.
* The \<p\> tags are showing on this page as well… I know the content will change but just pointing it out :PThe \<p\> tags are showing on this page as well… I know the content will change but just pointing it out :P

Members Home \- [http://miclubrd8.miclub.com.au/cms/members\-home/](http://miclubrd8.miclub.com.au/cms/members-home/)Members Home \- [http://miclubrd8.miclub.com.au/cms/members\-home/](http://miclubrd8.miclub.com.au/cms/members-home/)

* The slider changes height \(based on image, obvs\) which I think Cleen didn’t like on Concord, so maybe look at changing it or making it a certain height if that can be done??The slider changes height \(based on image, obvs\) which I think Cleen didn’t like on Concord, so maybe look at changing it or making it a certain height if that can be done??
* Maybe add a margin below the heading for each news post?Maybe add a margin below the heading for each news post?
* No weather on this page?No weather on this page?

News post pages – \(eg[http://miclubrd8.miclub.com.au/cms/2017/03/golf\-premier\-league\-update/\)](http://miclubrd8.miclub.com.au/cms/2017/03/golf-premier-league-update/%29)News post pages – \(eg[http://miclubrd8.miclub.com.au/cms/2017/03/golf\-premier\-league\-update/\)](http://miclubrd8.miclub.com.au/cms/2017/03/golf-premier-league-update/%29)

* Maybe remove the “Recent News” section from the sidebar? Its already kinda long with the archives there too and it will just keep growing.Maybe remove the “Recent News” section from the sidebar? Its already kinda long with the archives there too and it will just keep growing.
* The categories run into eachother on this page \-[http://miclubrd8.miclub.com.au/cms/2017/02/competition\-prize\-matrix\-for\-club\-competition/](http://miclubrd8.miclub.com.au/cms/2017/02/competition-prize-matrix-for-club-competition/). They don’t seem to do it on others that I’ve looked at. Weird.The categories run into eachother on this page \-[http://miclubrd8.miclub.com.au/cms/2017/02/competition\-prize\-matrix\-for\-club\-competition/](http://miclubrd8.miclub.com.au/cms/2017/02/competition-prize-matrix-for-club-competition/). They don’t seem to do it on others that I’ve looked at. Weird.

Category page – \(eg[http://miclubrd8.miclub.com.au/cms/category/members\-news/\)](http://miclubrd8.miclub.com.au/cms/category/members-news/%29)Category page – \(eg[http://miclubrd8.miclub.com.au/cms/category/members\-news/\)](http://miclubrd8.miclub.com.au/cms/category/members-news/%29)

* There’s no fallback image for news items.There’s no fallback image for news items.
* Page 2 etc shows me the same news items as page 1.Page 2 etc shows me the same news items as page 1.

Archives page – \(eg[http://miclubrd8.miclub.com.au/cms/2016/08/\)](http://miclubrd8.miclub.com.au/cms/2016/08/%29)

Archives page – \(eg[http://miclubrd8.miclub.com.au/cms/2016/08/\)](http://miclubrd8.miclub.com.au/cms/2016/08/%29)

* This seems to be broken as it’s giving me all of the posts.This seems to be broken as it’s giving me all of the posts.

My Information \-[http://miclubrd8.miclub.com.au/members/portal.msp](http://miclubrd8.miclub.com.au/members/portal.msp)My Information \-[http://miclubrd8.miclub.com.au/members/portal.msp](http://miclubrd8.miclub.com.au/members/portal.msp)

* The font sizes in the boxes are different. Can they all be the same size/line height?The font sizes in the boxes are different. Can they all be the same size/line height?

Edit Details \- [http://miclubrd8.miclub.com.au/members/details/edit.member.xsp](http://miclubrd8.miclub.com.au/members/details/edit.member.xsp)Edit Details \- [http://miclubrd8.miclub.com.au/members/details/edit.member.xsp](http://miclubrd8.miclub.com.au/members/details/edit.member.xsp)

* The heading cell font is Verdana, which looks gross. Can you make it Roboto like the rest of the site? \(this is the same on a few of the other product pages too\)The heading cell font is Verdana, which looks gross. Can you make it Roboto like the rest of the site? \(this is the same on a few of the other product pages too\)

Account \- [http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances](http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances)Account \- [http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances](http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances)

* The table is spaced out too much – I know it’s coz it’s wrapped in a \<center\> tag \(Product coding like it’s 1999\), but maybe try and make it full width? I did some sorcery for Deluxe but having logged in and had a look at it, some things need fixing \(such as white links on white BG’s… eek\) so maybe look at that? Product is fun to work with.The table is spaced out too much – I know it’s coz it’s wrapped in a \<center\> tag \(Product coding like it’s 1999\), but maybe try and make it full width? I did some sorcery for Deluxe but having logged in and had a look at it, some things need fixing \(such as white links on white BG’s… eek\) so maybe look at that? Product is fun to work with.

MiStats \- [http://miclubrd8.miclub.com.au/members/miStats.msp](http://miclubrd8.miclub.com.au/members/miStats.msp)MiStats \- [http://miclubrd8.miclub.com.au/members/miStats.msp](http://miclubrd8.miclub.com.au/members/miStats.msp)

* Left\-hand nav links changed colour. I’m sure it’s bootstrap to blame.Left\-hand nav links changed colour. I’m sure it’s bootstrap to blame.

Responsive \(home\)Responsive \(home\)

NavNav

* Below 1200px and before mobile menu kicks in \- spills onto next line and current nav items go outside the nav confines.
    Below 1200px and before mobile menu kicks in \- spills onto next line and current nav items go outside the nav confines.
* Mobile view \- Members and Book Golf buttons are different sizes.
    Mobile view \- Members and Book Golf buttons are different sizes.
* Mobile view \- arrow buttons – they don’t quite line up with the bottom borders \(they are a few px too tall on ipad and mobile portrait res\).
    Mobile view \- arrow buttons – they don’t quite line up with the bottom borders \(they are a few px too tall on ipad and mobile portrait res\).
* Mobile view – header takes up a lot of page space on both landscape and portrait mobile.Mobile view – header takes up a lot of page space on both landscape and portrait mobile.

Slider Slider 

* Tablet 960px – contact details aren’t viewable below 960px \(Upcoming Events region is hiding them\). Maybe hide the contact details at this size? They’re in the footer anyway.Tablet 960px – contact details aren’t viewable below 960px \(Upcoming Events region is hiding them\). Maybe hide the contact details at this size? They’re in the footer anyway.

Two Championship CoursesTwo Championship Courses

* Tablet \(960\) \- More spacing below this section \(before posts\) as they run into eachother.
    Tablet \(960\) \- More spacing below this section \(before posts\) as they run into eachother.
* Tablet \(960\) – Posts’ set height means random spacing – set this to auto. It’s also cutting off parts of the text/buttons.Tablet \(960\) – Posts’ set height means random spacing – set this to auto. It’s also cutting off parts of the text/buttons.

* Tablet \(960\) \- The figures \(36 holes etc\) – this doesn’t look great at full width \(plus encourages scrolling\). After trying to make it look good in the inspector, I think maybe just hide this bit as it’s not necessary. Otherwise make it full width and float the li’s across the page \(add padding of course\)?
    Tablet \(960\) \- The figures \(36 holes etc\) – this doesn’t look great at full width \(plus encourages scrolling\). After trying to make it look good in the inspector, I think maybe just hide this bit as it’s not necessary. Otherwise make it full width and float the li’s across the page \(add padding of course\)?

Upcoming Events Upcoming Events 

* Tablet \(770px and below\) – the border doesn’t sit at the bottom of the news posts. Images are squished – maybe show posts at 50% width or full width at this size? They’re also overflowing into the newsletter signup. Add height: auto to errythang post related.
    Tablet \(770px and below\) – the border doesn’t sit at the bottom of the news posts. Images are squished – maybe show posts at 50% width or full width at this size? They’re also overflowing into the newsletter signup. Add height: auto to errythang post related.
* Mobile \(600px and below\) \-  Images are breaking the layout.Mobile \(600px and below\) \-  Images are breaking the layout.

Newsletter Sign UpNewsletter Sign Up

* Tablet \(960 and below\) – the form and button have an awkward distance between them.
    Tablet \(960 and below\) – the form and button have an awkward distance between them.
* Mobile – Form field should be the full width of this area and the button should sit below. I think the button is fine where it is but make the form field wider/reduce the spacing between it and the button.Mobile – Form field should be the full width of this area and the button should sit below. I think the button is fine where it is but make the form field wider/reduce the spacing between it and the button.

Get in Touch Get in Touch 

* Tablet \(960 and below\) – the “Name” field is longer than the others.
    Tablet \(960 and below\) – the “Name” field is longer than the others.
* Mobile \- Doesn’t have enough space above the heading.Mobile \- Doesn’t have enough space above the heading.

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* Tablet/Mobile – make all the body fonts the same size.Tablet/Mobile – make all the body fonts the same size.

Single Course View – \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1/%29)Single Course View – \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1/%29)

* Mobile – The par/distance/index doesn’t align nicely.Mobile – The par/distance/index doesn’t align nicely.

Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)

* Tablet \(960\) – the image kinda runs into the left\-nav. Maybe add space between the left nav and the content section?Tablet \(960\) – the image kinda runs into the left\-nav. Maybe add space between the left nav and the content section?

* Mobile – Green fees table breaks layout.Mobile – Green fees table breaks layout.

Membership \- [http://miclubrd8.miclub.com.au/cms/membership/](http://miclubrd8.miclub.com.au/cms/membership/)Membership \- [http://miclubrd8.miclub.com.au/cms/membership/](http://miclubrd8.miclub.com.au/cms/membership/)

* Mobile – some of the post/section layouts are hidden – is there a height set on these?Mobile – some of the post/section layouts are hidden – is there a height set on these?

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

* Tablet and Mobile – when these are floated into columns of 2, the third \(of every 3\) doesn’t have another item floated to the right of it.Tablet and Mobile – when these are floated into columns of 2, the third \(of every 3\) doesn’t have another item floated to the right of it.

Portal \(mobile\) – some of the product pages are breaking on mobile view.Portal \(mobile\) – some of the product pages are breaking on mobile view.

* Change password on login – this isn’t styled.Change password on login – this isn’t styled.
* My Information – layout breaks. There’s a lot of excess space between the banner and heading.My Information – layout breaks. There’s a lot of excess space between the banner and heading.
* My Account – layout breaks.My Account – layout breaks.
* Eclectic – layout breaksEclectic – layout breaks

---

Some design feedback to start you off… there will be more but I’m afraid of my email not savingJ Haven’t looked at responsiveness yet.

Layout/Home

* Contact details are a lil hard to read on the bottom of the slider. Maybe increase the font weight to 300 or 400, as you want it to be subtle but also readable.
* Not keen on the underlines appearing on the dropdown nav links when you hover over them \(Paula said the same\!\) – maybe change the link colour on hover or add a coloured background behind that link \(as I did on the Deluxe site\).
* Make the active page/page parent link stand out from the rest of the nav links – either have the same styling as the active state or maybe a different colour for the active link?
* The “Upcoming Events” area looks good, but maybe have an easier way to distinguish the current event selected – at the moment, there’s just a small border below the event, and it’s hard to see as the bottom of the home section has a border too. Maybe make the colour lighter if keeping the event’s border?
* The “History of Thirteenth Beach” title/content – I feel like the text/heading/button are bumped up too close to the image. Maybe add some padding to the left of them. Also, I played around with the Inspector and made the text slightly smaller/increased the line height and thought it looked more balanced. Here’s what I did to the \<em\> \(I have no idea why it’s coloured and styled in caps though\). Also, make the button text slightly smaller and increase the padding around it.
*     font\-size: 28px;
*     line\-height: 135%;
*     padding\-right: 140px;
*     margin\-top: 40px;
*     display: block;
*     padding\-left: 20px;
* Upcoming Events – add a bottom margin under the headings for each post. Maybe increase the line height slightly? \(another size up\). Make the “Another fantastic… etc” text slightly smaller too.
* Footer – the line height for the address can be increased. The headings slightly smaller. Not keen on the gold email link \(as well as there’s an extra “t” in .net\)
* Left\-hand menu – not keen on the left\-hand border on active pages. Add in whatever styling you do for the navigation hovers/active items to keep the design consistent.

Golf \-[http://miclubrd8.miclub.com.au/cms/golf/](http://miclubrd8.miclub.com.au/cms/golf/)

* Yeah, the images should be the same height

Course Tour \-[http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* The heading for “36 Spectacular… etc” is smaller than the “Creek Course” and “Beach Course” sections… kinda not sold on this, especially as I can see them both on the page at the same time. Increase the space above the split section too as it kinda all runs into eachother.

Creek Course \-[http://miclubrd8.miclub.com.au/cms/golf/course\-tour/creek\-course/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/creek-course/)

* I like this layout. I’ll probably steal it at some point

Course Tour – Single Hole pages \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1\-2/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1-2/%29)

* More space between the video and the birdseye image would be nice. Also, the video has a lot of black space around it – not much we can do. Maybe make video taller?

Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)

* Reduce table cell padding. Keep the \<hr\>’s or borders under sections consistent too \(the one under the table isn’t as thick\)

Golf Shop \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-shop/](http://miclubrd8.miclub.com.au/cms/golf/golf-shop/)

* I like the heavy heading paragraph with the regular text. Maybe just add some extra space below the heavy text though.

Accommodation \-[http://miclubrd8.miclub.com.au/cms/accomodation/](http://miclubrd8.miclub.com.au/cms/accomodation/)

* More space between image and text would look great. Also, is onsite two words?

**From:** Michael Hiley \<michaelh@miclub.com.au\>

**Date:** Wednesday, 15 March 2017 at 12:47 pm

**To:** Verushka Ciprian \<verushka@miclub.com.au\>

**Cc:** Colleen Ballantyne \<colleen@miclub.com.au\>

**Subject:** Thirteenthbeach QA

---

New Email:

The rest of the design shiz… and then  some\!The rest of the design shiz… and then  some\!

* ~~When you hover over the nav items, some aren’t centred as in there’s not equal space around the word behind the blue background.When you hover over the nav items, some aren’t centred as in there’s not equal space around the word behind the blue background.~~
* The “History of…” text on the home page has turned black.The “History of…” text on the home page has turned black.
* Home page \- “Sign up to our newsletter” looks oddHome page \- “Sign up to our newsletter” looks odd

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* The “Creek Course” and “Beach Course” copy has turned blue/got larger.The “Creek Course” and “Beach Course” copy has turned blue/got larger.

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

* The links aren’t opening in a new window \(this content looks final, so just pointing it outThe links aren’t opening in a new window \(this content looks final, so just pointing it out

Contact \- [http://miclubrd8.miclub.com.au/cms/contact\-us/](http://miclubrd8.miclub.com.au/cms/contact-us/)Contact \- [http://miclubrd8.miclub.com.au/cms/contact\-us/](http://miclubrd8.miclub.com.au/cms/contact-us/)

* Maybe remove the blue background from the “Sign up to our newsletter” bit on the form? It looks a lil odd.Maybe remove the blue background from the “Sign up to our newsletter” bit on the form? It looks a lil odd.
* The \<p\> tags are showing on this page as well… I know the content will change but just pointing it out :PThe \<p\> tags are showing on this page as well… I know the content will change but just pointing it out :P

Members Home \- [http://miclubrd8.miclub.com.au/cms/members\-home/](http://miclubrd8.miclub.com.au/cms/members-home/)Members Home \- [http://miclubrd8.miclub.com.au/cms/members\-home/](http://miclubrd8.miclub.com.au/cms/members-home/)

* The slider changes height \(based on image, obvs\) which I think Cleen didn’t like on Concord, so maybe look at changing it or making it a certain height if that can be done??The slider changes height \(based on image, obvs\) which I think Cleen didn’t like on Concord, so maybe look at changing it or making it a certain height if that can be done??
* Maybe add a margin below the heading for each news post?Maybe add a margin below the heading for each news post?
* No weather on this page?No weather on this page?

News post pages – \(eg[http://miclubrd8.miclub.com.au/cms/2017/03/golf\-premier\-league\-update/\)](http://miclubrd8.miclub.com.au/cms/2017/03/golf-premier-league-update/%29)News post pages – \(eg[http://miclubrd8.miclub.com.au/cms/2017/03/golf\-premier\-league\-update/\)](http://miclubrd8.miclub.com.au/cms/2017/03/golf-premier-league-update/%29)

* Maybe remove the “Recent News” section from the sidebar? Its already kinda long with the archives there too and it will just keep growing.Maybe remove the “Recent News” section from the sidebar? Its already kinda long with the archives there too and it will just keep growing.
* The categories run into eachother on this page \-[http://miclubrd8.miclub.com.au/cms/2017/02/competition\-prize\-matrix\-for\-club\-competition/](http://miclubrd8.miclub.com.au/cms/2017/02/competition-prize-matrix-for-club-competition/). They don’t seem to do it on others that I’ve looked at. Weird.The categories run into eachother on this page \-[http://miclubrd8.miclub.com.au/cms/2017/02/competition\-prize\-matrix\-for\-club\-competition/](http://miclubrd8.miclub.com.au/cms/2017/02/competition-prize-matrix-for-club-competition/). They don’t seem to do it on others that I’ve looked at. Weird.

Category page – \(eg[http://miclubrd8.miclub.com.au/cms/category/members\-news/\)](http://miclubrd8.miclub.com.au/cms/category/members-news/%29)Category page – \(eg[http://miclubrd8.miclub.com.au/cms/category/members\-news/\)](http://miclubrd8.miclub.com.au/cms/category/members-news/%29)

* There’s no fallback image for news items.There’s no fallback image for news items.
* Page 2 etc shows me the same news items as page 1.Page 2 etc shows me the same news items as page 1.

Archives page – \(eg[http://miclubrd8.miclub.com.au/cms/2016/08/\)](http://miclubrd8.miclub.com.au/cms/2016/08/%29)

Archives page – \(eg[http://miclubrd8.miclub.com.au/cms/2016/08/\)](http://miclubrd8.miclub.com.au/cms/2016/08/%29)

* This seems to be broken as it’s giving me all of the posts.This seems to be broken as it’s giving me all of the posts.

My Information \-[http://miclubrd8.miclub.com.au/members/portal.msp](http://miclubrd8.miclub.com.au/members/portal.msp)My Information \-[http://miclubrd8.miclub.com.au/members/portal.msp](http://miclubrd8.miclub.com.au/members/portal.msp)

* The font sizes in the boxes are different. Can they all be the same size/line height?The font sizes in the boxes are different. Can they all be the same size/line height?

Edit Details \- [http://miclubrd8.miclub.com.au/members/details/edit.member.xsp](http://miclubrd8.miclub.com.au/members/details/edit.member.xsp)Edit Details \- [http://miclubrd8.miclub.com.au/members/details/edit.member.xsp](http://miclubrd8.miclub.com.au/members/details/edit.member.xsp)

* The heading cell font is Verdana, which looks gross. Can you make it Roboto like the rest of the site? \(this is the same on a few of the other product pages too\)The heading cell font is Verdana, which looks gross. Can you make it Roboto like the rest of the site? \(this is the same on a few of the other product pages too\)

Account \- [http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances](http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances)Account \- [http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances](http://miclubrd8.miclub.com.au/members/accounts/DisplayAccount?doAction=balances)

* The table is spaced out too much – I know it’s coz it’s wrapped in a \<center\> tag \(Product coding like it’s 1999\), but maybe try and make it full width? I did some sorcery for Deluxe but having logged in and had a look at it, some things need fixing \(such as white links on white BG’s… eek\) so maybe look at that? Product is fun to work with.The table is spaced out too much – I know it’s coz it’s wrapped in a \<center\> tag \(Product coding like it’s 1999\), but maybe try and make it full width? I did some sorcery for Deluxe but having logged in and had a look at it, some things need fixing \(such as white links on white BG’s… eek\) so maybe look at that? Product is fun to work with.

MiStats \- [http://miclubrd8.miclub.com.au/members/miStats.msp](http://miclubrd8.miclub.com.au/members/miStats.msp)MiStats \- [http://miclubrd8.miclub.com.au/members/miStats.msp](http://miclubrd8.miclub.com.au/members/miStats.msp)

* Left\-hand nav links changed colour. I’m sure it’s bootstrap to blame.Left\-hand nav links changed colour. I’m sure it’s bootstrap to blame.

Responsive \(home\)Responsive \(home\)

NavNav

* Below 1200px and before mobile menu kicks in \- spills onto next line and current nav items go outside the nav confines.
    Below 1200px and before mobile menu kicks in \- spills onto next line and current nav items go outside the nav confines.
* Mobile view \- Members and Book Golf buttons are different sizes.
    Mobile view \- Members and Book Golf buttons are different sizes.
* Mobile view \- arrow buttons – they don’t quite line up with the bottom borders \(they are a few px too tall on ipad and mobile portrait res\).
    Mobile view \- arrow buttons – they don’t quite line up with the bottom borders \(they are a few px too tall on ipad and mobile portrait res\).
* Mobile view – header takes up a lot of page space on both landscape and portrait mobile.Mobile view – header takes up a lot of page space on both landscape and portrait mobile.

Slider Slider 

* Tablet 960px – contact details aren’t viewable below 960px \(Upcoming Events region is hiding them\). Maybe hide the contact details at this size? They’re in the footer anyway.Tablet 960px – contact details aren’t viewable below 960px \(Upcoming Events region is hiding them\). Maybe hide the contact details at this size? They’re in the footer anyway.

Two Championship CoursesTwo Championship Courses

* Tablet \(960\) \- More spacing below this section \(before posts\) as they run into eachother.
    Tablet \(960\) \- More spacing below this section \(before posts\) as they run into eachother.
* Tablet \(960\) – Posts’ set height means random spacing – set this to auto. It’s also cutting off parts of the text/buttons.Tablet \(960\) – Posts’ set height means random spacing – set this to auto. It’s also cutting off parts of the text/buttons.

* Tablet \(960\) \- The figures \(36 holes etc\) – this doesn’t look great at full width \(plus encourages scrolling\). After trying to make it look good in the inspector, I think maybe just hide this bit as it’s not necessary. Otherwise make it full width and float the li’s across the page \(add padding of course\)?
    Tablet \(960\) \- The figures \(36 holes etc\) – this doesn’t look great at full width \(plus encourages scrolling\). After trying to make it look good in the inspector, I think maybe just hide this bit as it’s not necessary. Otherwise make it full width and float the li’s across the page \(add padding of course\)?

Upcoming Events Upcoming Events 

* Tablet \(770px and below\) – the border doesn’t sit at the bottom of the news posts. Images are squished – maybe show posts at 50% width or full width at this size? They’re also overflowing into the newsletter signup. Add height: auto to errythang post related.
    Tablet \(770px and below\) – the border doesn’t sit at the bottom of the news posts. Images are squished – maybe show posts at 50% width or full width at this size? They’re also overflowing into the newsletter signup. Add height: auto to errythang post related.
* Mobile \(600px and below\) \-  Images are breaking the layout.Mobile \(600px and below\) \-  Images are breaking the layout.

Newsletter Sign UpNewsletter Sign Up

* Tablet \(960 and below\) – the form and button have an awkward distance between them.
    Tablet \(960 and below\) – the form and button have an awkward distance between them.
* Mobile – Form field should be the full width of this area and the button should sit below. I think the button is fine where it is but make the form field wider/reduce the spacing between it and the button.Mobile – Form field should be the full width of this area and the button should sit below. I think the button is fine where it is but make the form field wider/reduce the spacing between it and the button.

Get in Touch Get in Touch 

* Tablet \(960 and below\) – the “Name” field is longer than the others.
    Tablet \(960 and below\) – the “Name” field is longer than the others.
* Mobile \- Doesn’t have enough space above the heading.Mobile \- Doesn’t have enough space above the heading.

Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)Course Tour \- [http://miclubrd8.miclub.com.au/cms/golf/course\-tour/](http://miclubrd8.miclub.com.au/cms/golf/course-tour/)

* Tablet/Mobile – make all the body fonts the same size.Tablet/Mobile – make all the body fonts the same size.

Single Course View – \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1/%29)Single Course View – \(eg[http://miclubrd8.miclub.com.au/cms/course/hole\-1/\)](http://miclubrd8.miclub.com.au/cms/course/hole-1/%29)

* Mobile – The par/distance/index doesn’t align nicely.Mobile – The par/distance/index doesn’t align nicely.

Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)Golf Information \-[http://miclubrd8.miclub.com.au/cms/golf/golf\-information/](http://miclubrd8.miclub.com.au/cms/golf/golf-information/)

* Tablet \(960\) – the image kinda runs into the left\-nav. Maybe add space between the left nav and the content section?Tablet \(960\) – the image kinda runs into the left\-nav. Maybe add space between the left nav and the content section?

* Mobile – Green fees table breaks layout.Mobile – Green fees table breaks layout.

Membership \- [http://miclubrd8.miclub.com.au/cms/membership/](http://miclubrd8.miclub.com.au/cms/membership/)Membership \- [http://miclubrd8.miclub.com.au/cms/membership/](http://miclubrd8.miclub.com.au/cms/membership/)

* Mobile – some of the post/section layouts are hidden – is there a height set on these?Mobile – some of the post/section layouts are hidden – is there a height set on these?

Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)Reciprocal Clubs \-[http://miclubrd8.miclub.com.au/cms/membership/reciprocal\-clubs/](http://miclubrd8.miclub.com.au/cms/membership/reciprocal-clubs/)

* Tablet and Mobile – when these are floated into columns of 2, the third \(of every 3\) doesn’t have another item floated to the right of it.Tablet and Mobile – when these are floated into columns of 2, the third \(of every 3\) doesn’t have another item floated to the right of it.

Portal \(mobile\) – some of the product pages are breaking on mobile view.Portal \(mobile\) – some of the product pages are breaking on mobile view.

* Change password on login – this isn’t styled.Change password on login – this isn’t styled.
* My Information – layout breaks. There’s a lot of excess space between the banner and heading.My Information – layout breaks. There’s a lot of excess space between the banner and heading.
* My Account – layout breaks.My Account – layout breaks.
* Eclectic – layout breaksEclectic – layout breaks
