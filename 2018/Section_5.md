# Section 5

**Spread / Rest Perameter**: **\[...\]** used for putting an array in to another array. 

* * *  e.g.:
        * let a = \[2,3,4\];
            let b = \[1, ...a, 5\];
* function collect\(...a\) {

     console.log\(a\);

}

collect\(1,2,3,4,5\);
