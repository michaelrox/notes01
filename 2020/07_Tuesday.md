# 07 Tuesday

1. ~~Follow up ABD Skate~~
2. ~~Test auto translation of Langify, if it's still shit then judge which is best based on design and try to export from Weglot...~~
    ~~FCS~~ 
3. ~~Update timeline estimate~~
    Write agenda for tomorrow.
    Fix items in QA
    Test theme edited by bold
    Update Navigation to be 99% good
    Plan meeting tomorrow 100%.
    Deus
4. Test send data through to existing doc
    Install app on dev site properly
    MCM

---

Deus timeline:

[https://docs.google.com/spreadsheets/d/1RWvkIzKF1W01NR5XR4H3MV\-4Swq28TE4aD23k3j\_48E/](https://docs.google.com/spreadsheets/d/1RWvkIzKF1W01NR5XR4H3MV-4Swq28TE4aD23k3j_48E)

---

Current project status

* Total project: 78% \(in week 32 out of about 41\)
* Built around 60% of theme. 
* Left to do is: 
    * Finish theme \(about 4/5 weeks\)
        * Custom app
        * Deus Locations
        * Generic pages and pop ups
        * Events
        * Additional features added or overlooked in original scope
        * Fine detail updates
        * Music Category Page
    * QA and Migrate Data \(about 4 weeks\)

Areas of problems and surprise

1. Pick out the "Can't do's" in the spreadsheet you and I have been using to log comms.
2. Pick out the "Low priorities" that you and I discussed last week.
3. Areas which I misinterpreted, or overlooked when originally scoping

Updates to timeline

1. Will need some time extended, and proposing dropping the Music section for launch as that will take around 1 week.

---

re\-aligned the plan.

Key reasons for requiring extension:

Custom App build: Was supposed to begin asap. App takes 3 weeks, and 1 additional week is needed after.

Added or overlooked in Scope: There is about 9 business days worth of work which I misinterpreted or overlooked when scoping original timeline.

Music Section

If app can be green\-lit to start on Monday, then project will be pushed back 21 days

Want to propose launching without Music section, which would save 6 working days
