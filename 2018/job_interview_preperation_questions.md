# job interview preperation questions

1.  Tell me about your greatest accomplishment on the job.
2. Tell me about a difficult situation you encountered at work and how you overcame it.
3. Give me three adjectives you would use to describe yourself and examples of your work style to support them.
4. Give me three adjectives you would use to describe your weaknesses/areas for improvement and the reasons why you feel you need to work on these.
5. If I was to ask a co\-worker about you, how would they describe you on\-the\-job?
6. Tell me about a time where you were required to work with a difficult person \(i.e. client, co\-worker, manager, etc.\). How did you handle the situation? What was the outcome?
7. If you were given a task to complete in an unrealistic time frame, what would you do?
8. If someone came to you with an enthusiastic, yet unrealistic request, how would you handle it?
9. Why are you looking to leave your current position?
10. What do you want to get out of your next job?
11. Tell me about the best manager you ever had? Now tell me about the worst?
12. What are the three most valuable things you’ve learned while working in your current position?
13. What was the biggest mistake you’ve made on a job? How did you handle the failure?
14. What do you feel makes you successful in your current role? Give an example to support your success.
15. What do you think makes a company good to work for? What do you like about your current company? What could be better?
16. Do you prefer working alone or in teams? Give examples of how you have worked successfully both alone and in a team? What do you attribute your success to?
17. What are your own business philosophies – what do you feel must be present in a successful business?
18. What skills are you looking to develop in your next job? Why?
