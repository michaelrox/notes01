# shop links

[https://help.shopify.com/themes/development/getting\-started/helpful\-resources](https://help.shopify.com/themes/development/getting-started/helpful-resources)

video: 190mins

[https://www.skillshare.com/classes/Shopify\-Essentials\-for\-Web\-Developers\-From\-Store\-Setup\-to\-Custom\-Themes/1070001866](https://www.skillshare.com/classes/Shopify-Essentials-for-Web-Developers-From-Store-Setup-to-Custom-Themes/1070001866)

advanced video: \- 90 mins

[https://www.skillshare.com/classes/Advanced\-Shopify\-Theme\-Development/708093439?via=similar\-classes](https://www.skillshare.com/classes/Advanced-Shopify-Theme-Development/708093439?via=similar-classes)
