# 17/08/11

|Tag                                                                       |Title         |Comment                                          |Time|Pr|
|--------------------------------------------------------------------------|--------------|-------------------------------------------------|----|--|
|[DSC\-84](http://jira.mediasphere.com.au:8080/browse/DSC-84)              |Breadcrumb    |Tasked to Dean                                   |?   |  |
|[SERVICEIQA\-47](http://jira.mediasphere.com.au:8080/browse/SERVICEIQA-47)|CKEDit padding|writing back, need more info                     |?   |  |
|[LEARNINGFO\-16](http://jira.mediasphere.com.au:8080/browse/LEARNINGFO-16)|Graphic change|replace an image                                 |10m |1 |
|[EALDHUBMAS\-28](http://jira.mediasphere.com.au:8080/browse/EALDHUBMAS-28)|Access        |still need accordion                             |4   |5 |
|QLD EALDHUB                                                               |99%           |all changes made from above, except for accordion|    |  |
|[DSC\-83](http://jira.mediasphere.com.au:8080/browse/DSC-83)              |logo alt text |asked for feature request                        |    |  |
|                                                                          |              |                                                 |    |  |
|                                                                          |              |                                                 |    |  |

Core

|Tag                                                                   |Title                   |Comment|Time|Priority|
|----------------------------------------------------------------------|------------------------|-------|----|--------|
|[LMSCORE\-647](http://jira.mediasphere.com.au:8080/browse/LMSCORE-647)|responsive buttons      |       |    |        |
|[LMSCORE\-646](http://jira.mediasphere.com.au:8080/browse/LMSCORE-646)|cart buttons            |       |    |        |
|[LMSCORE\-753](http://jira.mediasphere.com.au:8080/browse/LMSCORE-753)|accordion or tabs in 5.3|       |    |        |
|[LMSCORE\-768](http://jira.mediasphere.com.au:8080/browse/LMSCORE-768)|Calendar                |       |1   |        |
|accordions                                                            |                        |       |    |        |

Powerhouse website Phase 1

|task          |Comment                    |time\(hr\)|step|
|--------------|---------------------------|----------|----|
|QA            |2 hours, 6 hours for fixing|8         |2   |
|              |                           |          |    |
|Responsive css|                           |2         |1   |

Powerhouse website Phase 2

|task                           |time\(hr\)|step|
|-------------------------------|----------|----|
|CMS integration                |12        |1   |
|Video integration              |2         |2   |
|Content \- Phase 2 \(10 pages\)|12        |3   |
|Blog posts \- 5 posts          |5         |4   |
