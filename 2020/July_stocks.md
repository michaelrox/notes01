# July stocks

$2,500

\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-\-

VESG \- $1000 \- Done

---

Interested:

**KGN:**

Will continue to grow, but in 10 years will Amazon etc. take over?

**CSL**:

Will always grow over net 10 years.

Is it ethical? There

**A2M:**

Always going to grow, but is it too slow?

**APT**:

Have I missed the opportunity?

**SZL**

**PPH**:

No, because I don't believe church will be worth \+$1b in 10 years like it is today.

**XRO**

Solid, consistent growth over 7 years...

Not sure if this will reach saturation...

**DDR**

Speculative \- IT hardware... Solid growth last few years, I can't see why this won't continue...

**WES**

Slow growth, but will grow... 

If it hits $30 then buy.
