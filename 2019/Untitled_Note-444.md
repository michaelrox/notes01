# Untitled Note

`Mr Poolman`

Running Strategic Doc summary

Thursday, 14 February 2019

7:57 AM

KPI Analysis

1. KPI \- Same visitor count, but a larger percent of them end up purchasing
2. AOV \- Average Order Value
3. RPV \- Revenure per visitor
4. Visitors \- both new and returning

 

 

 

Benchmark \- 1/10/18 \- 12/12/18

|KPI

                                                                                    |Your Store

|Industry Benchmarks

 

                        |
|---------------------------------------------------------------------------------------|----------|--------------------------------------------|
|Revenue Per Visitor

\(Sales / total sessions\)

                                          |$5.47

     |Average \- $1.50

Good \- $2

Excellent \- $3\+

 |
|Conversion Rate

\(Orders / sessions\)

                                                   |3.80%

     |Average \- 1.5%

Good \- 2.5%

Excellent \- 4%

  |
|Average Order Value

\(Sales / number of transactions\)

                                  |$143.80

   |Average \- $50

Good \- $75

Excellent \- $100\+

|
|Abandoned Cart Rate

1\- \(sessions with a transaction / sessions that had an add to cart

|34%

       |Average between 55% \- 75%

                  |

 

> My opinion is that this data shows we need to increase number of sessions/visitors. There should be a sweet spot where the stats above are lower, but the sessions are higher meaning more money over\-all.

> 

> Another note \- I need to check out other competitors and Shopify based competitors. Example: :Zodiac

> 

 

Shopify recomends:

1. Upselling pop up in cart.
    * Shopify doesn't reccomend ajax cart because it removes a step, therefor removes an upsell opportunity.
    * Smar7 Bundle Upsell is an app that can make discounts if they customer bundles stuff \- $47 / month
    * Bold Brain is a real upsell that can upsell after the checkout

 

1. Upselling recommended products using 'ai' apps examples:
    1. Personalizer by LimeSpot \-Beginning Boutique use this
    2. Also Bought
    3. Personalized Recommendation
2. Bundles. Apps include:
    1. Frequently Bought Together
    2. Bundles | Also Bought together
    3. Bold Bundles
    4. Boost Sales \- Upsell Cross Sell
3. Warranty add\-on
    1. E.g. Frankies Bikinis
4. Badges
    1. Apps are a bit pricey
        1. Product Label By Freeh
        2. InstaBadge
5. Free shipping if spend over x
6. Bulky Shipping calculator
    1. mention weight of product and this explains to customer why shipping is expensive.
7. Transit Calculator
8. Product Zoom on Hover
    1. Do pool supplies need this?
9. Pre\-order stock

 

 

MPM feedback

1. Upsell in cart is yes
    1. Free shipping over x amount
    2. "complete the set" kind of thing to bundle products
        1. Frankies does this.
2. Product filters
    1. C

 

 

 

 

 

 

 

Created with Microsoft OneNote 2016.
