---
modified: 2021-12-01T08:18:11+10:00
---

# Shelving measurements

**Nursery**

**Closet**:

2m wide. 1m halfway

78cm \- sliding doors width

1.7m tall

1.36m tall to bottom of triangle.

42cm deep

92cm behind Door Width

9cm behind door when shut.

2.58m wall of door

3.5m blank wall

**Study nook:**

1.8m wall next to window

60cm. Desk deep

**Study room:**

1m X 1.29m \- closet

80cm. Between desk and open closet door

3.5m blank wall

1.79m between corner desk and door wall

1.98m between door and blank wall
Study wardrobe left gap: 2.4m tall, 40cm left, 50cm deep to coat.
