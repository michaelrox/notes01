# Performance Evaluation May 2018

* My greatest achievement is streamlining existing systems and improving management standards.
* I am working on more documents and systems to prepare for a bigger team and company
* My goal is to eventually become a team leader either creative director for HiSmile/Beedil etc. or to be lead website designer.
* I have been pro\-active in seeking out project management tools, optimizing website tools such as git, theme kit, etc.
    * Integrated the existing site with modern standards of version control \- about 20 hours of work in private time
    * Integrated Theme Kit \(a development environment\) to help make live changes faster \(from 3 hours to make live down to less than 5 seconds\)
    * Built the first 20 hours of the new website in my own time. Because of this Jordan started with a solid ground to build on. The new theme is set up to:
        * Have a standard CSS framework \(Bootstrap\) so new developers will find tasks easier and tidyer.
        * Increase page load times
        * Be easier to pin point redundant code 
* Teamwork \- Never saying no to requests for help. Putting in the time to teach people new skills and making up for lost time by coming in early and staying late regularly.
* Communication \- Daily summary with Monique about what I worked on and what is coming next. Also working closely with Dan and Cassie for projects in their departments, and also working closely with Alex for more detailed website projects.
* Culture \- Every week I'll try to organise a lunch with everyone, or an excuse to casually catch up with individuals. For example last week I put in some time after work to help teach Eden some things about investing. This week I helped Marcus with a minor website change on his personal website.
* Multi\-tasking \- When quoting work I measure my day as only 5 hours because I understand other departments often require help with something that is in the grey area of IT/Website and them. For examples I help the videographers with 
