# Southbound

wear:
Gold vest
Denim Cap
Hippy Pants

Plan:
times
create playlist
     \- learn songs bro\!

pack:
2 festival outfits
PJ's
Monday clothes
charger
go pro? mount?
snacks?
     \- tins \(tuna, oysters, beans\)
     \- crackers \- like 6 packs at least
     \- vitamins
     \- caffeine tablets
Sunblock
Chewing Gum

Brekky food
     \- sausages
     \- bacon
     \- eggs
     \- bread
     \- maybe pre\-make some chilli kinda thing?

Ear plugs
toilet paper
coffe? tea?
cash
