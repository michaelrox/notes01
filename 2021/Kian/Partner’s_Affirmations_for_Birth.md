# Partner’s Affirmations for Birth

`hypnobirthing`

I remain calm, confident, and focused as I support my partner through each phase of labor.
* I help my partner feel safe, secure, and deeply loved throughout labor
* I am confident and comfortable in our choices for our birth and in my ability to convey them to our caregivers.
* I trust and believe in the natural process of birth and remain present for my partner and baby throughout pregnancy and birth.
* I gently and effortlessly help my partner remain focused on her natural birthing instincts.
* I assert myself lovingly and confidently by reminding my partner to follow her body’s lead.
* I express love and encouragement easily and effortlessly through all phases of birth.
* I am completely comfortable and at ease with silence and giving my partner space as she follows her birthing instincts.
* My presence is significant during the birth of our baby; I trust my own instincts and know that all is well.
* I trust in the natural process of birth. I look forward to skin\-to\-skin bonding with our newborn baby.
* I deeply believe my partner is capable of birthing in a healthy, safe, and peaceful manner.
* I know that I will respond to her needs and requests with our baby’s best interest in mind.
* I know that my voice and loving participation will be the best care for my partner during labor and birth.
* I accept that my calm, loving presence during our baby’s birth is paramount to a good outcome.
* I take the time I need to prepare my heart and mind for parenting our new baby. My encouragement and tenderness keep my partner calm and peaceful during the birthing of our child. 
