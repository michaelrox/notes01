# Shaligram question

`Mizzie`

 

Tuesday, 19 March 2019

9:59 AM

Shaligram question

Hey, I have a odd question.

The client Mizzie \(who you recently fixed the WordPress bug for\) is moving their website to Shopify.

 

Is your team able to help with the copying and moving of content? It would be great if you could run the existing images through an image compressor before uploading like imageOptim, or tinypng.com

 

Can your design team use Shopify Customiser? Or would they need to make still mockups first? The theme we are using has a lot of pre\-built sections which we would rather utilise than edit the theme.

 

I understand quoting time may be difficult for this, would you be ok with say only doing 4 hours at a time?

 

Created with Microsoft OneNote 2016.
