# Calming the baby

* Calming the baby If the grandparents insist on winding your baby up, send them home. Otherwise get them to help you by joining in with this plan. Alternatively, if you have a toddler, they would be delighted to help by looking after her.
* Stick a note on the front door saying that you are out. Go into her room, draw the blinds and make the light in the room dim.
* Put on some quiet, restful music; that’s for you, but babies do like a background of ‘family sounds’ rather than silence.
* Get the baby and take her into the room with you. NEVER leave her on her own. Feed her on demand in the dim light, avoiding long periods of intense eye contact with her \(obviously you can look at her – but make your gaze calming\). 
* Put her on your chest with her ear against your heartbeat and cuddle her until she settles. This reminds her of the sounds and feel of the womb. 
* It may take some time. Be patient. You can now wear her on your chest for as long as you both like. Lie down with her on the bed, making sure that if you fall asleep that there are no risk factors for SIDS in your sleeping arrangements. Check out the risk factors beforehand.

The more he sleeps, the less opportunity there is for him to be stimulated, and the more he will sleep. This is a good cycle, not a vicious one. When he’s calm and sleepy, you can return him to the living room
