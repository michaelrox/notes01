# Fair work research

Spoke with someone from fair work 30/1/18 at 5:30pm

me:

* I have been told that I should work overtime unless specifically told not to. 
* I said I think it is best to work my specified hours unless they ask otherwise. The matter didn't really get a resolution so I'm concerned they ignored my point.
* I have been informed that there will be no compensation for overtime worked.

Fair work feedback:

* I get paid above minimum wage which means HiSmile may in the clear
* HiSmile doesn't have an _Enterprise Agreement_ that he could see
* I would probably come under the _Professional_ employees award due to having a degree in the computer field
    * I could maybe contact the _Australian Computer Society_ because of my field
* Job Watch could be worth calling \- 1800331617, and they will help me _apply legal rights_

**My further questions:**

* Can I refuse to work overtime every day?
* Can I eventually be fired for refusing to work overtime?

My questions for HiSmile:

* My work so far has been satisfactory, in fact Just said they are very pleased with my work \(Monday 22nd.\)

draft email about working late:

Hi,

I wish to respond to the discussion we had yesterday in which you stated I neglected my responsibilities on Monday the 29th of January by going home at 4:00 pm.

I disagreed to this, as I had sent a quote at \(x time\) for the time remaining in the task \(x hours\) and was not informed that the task was required to be finished by a certain date or time. My normal business hours are 8:00am to 4:00pm.

Your response was that I should always work overtime unless I have something going on at home. I don't believe this is a reasonable request. For this case in particular my example was that the task expected to be finished on Monday the 29th had already been going on for 

My concern with this statement is that it is not a reasonable request as more often than not, tasks that I am assigned will not be finished within a work day, can be quoted with reasonable accuracy, can be planned ahead of time, and should therefore very rarely require urgent overtime, and certainly not daily. Therefore my suggestion was that you inform me ahead of time when you will be requiring overtime or urgent work.

**Find Your Product** \- started 10am 23/1/18 to 4:00 30/1/18 \(still around 4 hours left as of 30/1/18 assuming no further changes, documented in bitbucket\)

---

**Maximum hours:**

[https://www.fairwork.gov.au/how\-we\-will\-help/templates\-and\-guides/fact\-sheets/minimum\-workplace\-entitlements/maximum\-weekly\-hours](https://www.fairwork.gov.au/how-we-will-help/templates-and-guides/fact-sheets/minimum-workplace-entitlements/maximum-weekly-hours)

What are the maximum weekly hours of work?

An employer must not request or require an employee to work more than the following hours of work in a week, **unless the additional hours are reasonable**:

**for a full\-time employee, 38 hours**

**An employee may refuse to work additional hours if they are unreasonable.**
