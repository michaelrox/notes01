# Untitled Note

`Mutt`

Mutt removed

Wednesday, 17 April 2019

1:17 PM

Global trackign:

 

| 

|Industry

                   |Tertiary

 |Low

   |N/A

                                                                                       |5.0

|5.0

|The industry is listed as home and garden and should be shopping.

                                                                                                                                                                                                        |
|-|---------------------------|---------|------|------------------------------------------------------------------------------------------|---|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|User capture

               |Secondary

|Low

   |Enabled

                                                                                   |3.0

|3.0

|Enabled but User\-ID not enabled. User\-ID is optimal but would require a GA360 account \(which is really expensive\) to use and make it worthwhile, so for where business is, current features are correct.

                                                             |
| 

|Remarketing

                |Tertiary

 |Medium

|Enabled

                                                                                   |5.0

|5.0

|Not enabled. Recommend enabling

                                                                                                                                                                                                                                          |
| 

|Session settings

           |Tertiary

 |Medium

|Sessions concluding after 30 minutes of inactivity and campaigns concluding after 6 months

|5.0

|5.0

|Ok.

                                                                                                                                                                                                                                                                      |
| 

|Referral Exclusion List

    |Primary

  |Medium

|Exclude all payment providers

                                                             |1.0

|1.0

|Within Referral exclusion list only Mutt's respective websites are excluded. All payment providers should be excluded. Consider making Mutt.com.au / Mutt.com excluded referrals and potentially even their own "sister referral sites" that are tracked separately.

     |
| 

|Adwords Linking

            |Tertiary

 |High

  |Linked

                                                                                    |5.0

|5.0

|Not linked.  Recommend linking.

                                                                                                                                                                                                                                          |
| 

|Exclude URL Query

          |Primary

  |High

  |Defined

                                                                                   |2.0

|2.0

|Recommend excluding any code in URL when tracking has been enabled so each page will only have one URL in GA.

                                                                                                                                                            |
| 

|Exclude BOT filtering

      |Tertiary

 |Medium

|Selected

                                                                                  |5.0

|5.0

|Not selected in GA, recommended that this is selected.

                                                                                                                                                                                                                   |
| 

|Time zone country/territory

|Tertiary

 |Medium

|Defined to local time and territory

                                                       |5.0

|5.0

|Sydney/LA respectively

                                                                                                                                                                                                                                                   |
| 

|Currency display

           |Tertiary

 |Medium

|Defined to local currency

                                                                 |5.0

|5.0

|The AU site has USD as the currency displayed as; it should be AUD for the AU site.

                                                                                                                                                                                      |
| 

|Site search tracking

       |Tertiary

 |High

  |Enabled with URL excluded from query parameters

                                           |5.0

|5.0

|Site search is not enabled. We recommend enabling it so we can get a sense of what terms people are searching. It comes in especially handy to recommend product and to offer misspelling opportunities

                                                                  |
| 

|Ecommerce enabled

          |Tertiary

 |High

  |Advanced ecommerce enabled

                                                                |5.0

|5.0

|Enhanced ecommerce enabled

                                                                                                                                                                                                                                               |
| 

|Conversion funnel capture

  |Tertiary

 |Medium

|Defined with each step of checkout tracked

                                                |5.0

|5.0

|Only place an order is identified as a goal, so we don't see where we might be losing customers in the checkout process. It would be good to set goals that include all the steps from clicking on checkout to filling out information to clicking on order confirmation.

|
| 

|Filters

                    |Primary

  |Medium

|Defined to exclude known internal & third party users

                                     |2.0

|2.0

|Filters should exclude internal office traffic, search servers, and other third parties that access the website regularly for better data accuracy.  There are no filters. We recommend adding PeriscoPe and home & business IP addresses for Mutt

                       |

 

 

Created with Microsoft OneNote 2016.
