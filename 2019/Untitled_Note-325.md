# Untitled Note

`JONES`

Theme selection

Monday, 1 July 2019

1:23 PM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|# Considerations:

 

2.

Motion

122 reviews \- 100% positive

performance rating: 65 / 100

* Design is modern and clean, but relies on good content.
    * Lots of options to add brand and product images and videos.
    * Content must be high quality
* In\-depth product page layout
* Has options for reviews on product page
* Can be fast if kept lightweight
* About:[https://themes.shopify.com/themes/motion/](https://themes.shopify.com/themes/motion/)

Cons:

* A little slow
* Stores using theme are inconsistent in speed

 

Split

74 reviews \- 100% positive

Performance rating: 42 / 100

* Clean design
* Unique interaction with the header slider
* Animations, although they are subtle
* Good for high quality images, requires consistent creative
* About \-[https://themes.shopify.com/themes/split/](https://themes.shopify.com/themes/split/)

Cons:

* Too slow
* Not enough features
* Stores using theme are inconsistent in speed

 

 

Turbo

* Third party, bespoke theme.
* Many consider this to be the industry standard 'all around' theme.
* [https://outofthesandbox.com/collections/turbo\-theme/products/turbo\-theme\-chicago](https://outofthesandbox.com/collections/turbo-theme/products/turbo-theme-chicago)
* Used by FCS & Softech

Cons:

* Too slow \(40/100\)
* Design is lacking attention to detail \(inconsistent padding etc.\)
* Stores using theme are inconsistent in speed

 

Fastor

* Third Party theme
* Best selling theme on Theme Forrest
* So many features
* [https://rt\-fastor\-default2.myshopify.com/](https://rt-fastor-default2.myshopify.com/)

Cons:

* Very slow \(11/100\)

 

Saleshunter

* In\-built page builder
* Lots of features
* Slow \(53/100\)

 

Precia or Agilis

* 88/100
* Third party theme
* [https://themeforest.net/item/precia\-responsive\-shopify\-sections\-theme\-google\-pagespeed\-99100\-upsell\-geoip\-currency/23835634](https://themeforest.net/item/precia-responsive-shopify-sections-theme-google-pagespeed-99100-upsell-geoip-currency/23835634)
* [https://themeforest.net/item/agilis\-sport\-responsive\-shopify\-sections\-theme/23611825](https://themeforest.net/item/agilis-sport-responsive-shopify-sections-theme/23611825)

 

Shella

[https://shella\-speed.myshopify.com/](https://shella-speed.myshopify.com/)

* Fast: 90/100
* Lots of features
    * Compare
    * Instant search
    * Wish List

 

* Only $44usd

Cons

* Third party \- so possibly poor support
* Search is poor, but can be fixed with an app

 

 

 

 

|
| 

|1.

[https://themes.shopify.com/themes/prestige/styles/allure](https://themes.shopify.com/themes/prestige/styles/allure)

* [https://tulefogcandles.com/](https://tulefogcandles.com/)
* [https://www.mlouye.com/](https://www.mlouye.com/)

* Fast: 99/100 and sometimes 100/100 \- unbelievable
* Shopify supported theme, has to be built to a standard
* Instant search

 

Cons:

* Design/Dev needs light touch\-ups
    * Some buttons styles need fixing

Heavily modified stores using the theme:

* Metathreads \(53/100\)
* Weiss \(100/100\)
* Smallwoodhome.com \(64/100\)

 

 

Set up preview theme with Prestige \- scores 94/100 from stock...

 

 

 

 

 

[https://themes.shopify.com/themes/colors/styles/pastel](https://themes.shopify.com/themes/colors/styles/pastel)

* [https://madewithspin.com/](https://madewithspin.com/)
* [https://ekzo.co/](https://ekzo.co/)
* [https://colors\-theme\-pastel.myshopify.com/](https://colors-theme-pastel.myshopify.com/)

* Fast: 92/100
* Shopify supported theme, has to be built to a standard

Stores using:

[https://arda\-wigs.com/](https://arda-wigs.com/) \- 96/100

Alexa \- 103,782        

 

[https://kixsquare.com/](https://kixsquare.com/) \- 65/100

Alexa \- 312,390

 

Custom dev required:

* Make icons unique
* Tidy up fonts to be professional
* Catalogue is inconsistent with most ecommerce stores
* Will take a lot of work to get up to scratch

 

 

 

 

Shella

[https://shella\-speed.myshopify.com/](https://shella-speed.myshopify.com/)

* Fast: 90/100
* Lots of features
    * Compare
    * Instant search
    * Wish List
* Only $44usd

Cons

* Third party \- so possibly poor support
* Search is poor, but can be fixed with an app
* Hard to find stores using it, and none with high Alexa ranking \(doesn't mean much, but helps to show real life usability\)

 

Live example:

[https://www.adoredvintage.com/](https://www.adoredvintage.com/)

96/100

Alexa: 512,186

 

 

 

 

 

|

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

 

Created with Microsoft OneNote 2016.
