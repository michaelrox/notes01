# Deus Import from Wordpress

Blog Feeder app:

[https://www.quora.com/How\-do\-I\-migrate\-WordPress\-to\-Shopify](https://www.quora.com/How-do-I-migrate-WordPress-to-Shopify)

someones review:

We used BlogFeeder to import ~800 posts from our old Wordpress site. Pretty neat. The support was good \(thanks Daniel \!\) and accomodating.

Tips if you have a whole Wordpress to import with many posts : during the validation of the feed, the app will most likely fall in timeout, due to Wordpress' duration of the RSS feed generation. To counter that, display the RSS feed in your browser, save it as a text file, upload it as a media on your Wordpress site and paste this new URL to the app. Will work like a breeze.
