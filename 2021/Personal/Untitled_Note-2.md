# Letter to Cath and James


Dear Cath and James,

I want you to know:

The love that you share together is wonderful.

Your journey so far has been inspiring.

Your happiness together is contagious.

James, 

You are respectful, supportive and adventurous

You've brought an abundance of joy in to Caths life.

You're a welcomed new brother.

Cath,

You are motivational, mindful, and dependable

You are an attentive friend and a caring sister

You'll be each others guiding stars to safety.. and danger

You'll encourage each other to learn... and forget

You'll always have love amongst each other, and from your family and friends.

The emotions you'll share will amplify you're experiences.

You are a perfect match for each other,

and it was always obvious to me that you would share the rest of your days together.

I will always guide you towards safety.

I promise to listen to you.

You will always have love from myself and Chantelle.
