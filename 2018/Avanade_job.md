# Avanade job

Description
 UI Developer

* Join a talented team of more than 200 UX professionals
* 80\+ hours per year allocated to developing your UX skills
* Work on high\-profile design projects across mobile/web
* 4 weeks annual leave plus 5 personal days per year
* $1,000 per year gadget allowance

 

Avanade Australia is growing its User Experience \(UX\) capability, and we’re looking for UI Developers to join the team. 

 

We’re a global team of digital and UX experts, with more than 20 UX designers and UI developers based across Australia, who help our customers create digital experiences that are usable, engaging, and beautiful.

 

Focusing on creating web/mobile apps and intranets, the role involves working with our UX Designers and Solution Architects, to design and code the User Interface \(UI\) on projects.

 

Role

 

We're looking for talented and passionate individuals with demonstrated skills in UI design and development – as well as a history working inside of development teams, and interacting with customers.

 

The role involves operating within a consultancy\-based environment, working on the UX stream\-of\-work within a project team. Strong technical skills are essential – as is an ability to quickly understand problems, and communicate effectively.

 

Qualifications
 Requirements

* A portfolio demonstrating UI design and front\-end development skills 
* Experience working on web/mobile apps and intranets \(not only informational websites\) 
* Strong UI development skills, including; HTML, CSS, and JavaScript \(jQuery\) 
* Strong UI design knowledge, including; Adobe products, accessibility, and web standards

 

 About

 

As a joint venture between Accenture and Microsoft, Avanade combines the best expertise from the consultancy and technology worlds. We have grown substantially every year since our inception in 2000, and we are now looking for the next generation of talent to help us grow even further.

 

## Primary Location

\- Asia Pacific\-Australia\-Perth

Region\- Australia

## Job

: Experience Design

Capability Group: SL \- Application Development
