# Untitled Note

`Tasks`

Rhythm Q's

Thursday, 30 May 2019

11:37 AM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |

|

|
|-|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|||
| 

|Project Overview:

1. What are the Periscope deliverables from a distance? Example:
    1. Shopify theme development
    2. WordPress theme development
    3. Email templates \- which platform
    4. Quality Assurance \- Designer \+ Project management combined
    5. Content Creation
    6. Content implementation
    7. Inventory management considerations?

 

1. If the above is phase 1, what is phase 2 and what's the expected timeline for that?
    1. Tandem project / custom clothes designer. \- More questions about this below.

 

1. Who will be the other external stakeholders in the project?
    1. UX and UI Design: Rhythm
    2. WordPress hosting?

 

1. How will collaboration work?
    1. Periscope suggest a weekly call during the entire project, including design.
    2. Periscope suggest communication over Azendoo

 

1. Are there specific items that Periscope won't be handling that could be mistakenly assumed? Examples:
    1. Shopify logistics set up
    2. Inventory management
    3. Design, content creation or
    4. Email flows integration and set up

 

 

Website development considerations

1. What is the name of the theme which your current store was built on? We are aware it has likely been heavily modified, but this will help with pointing out what exactly has changed.
2. Are there any specific Shopify apps that need to be integrated?
3. Do we need to consider Retailer/Wholesale pricing?
4. Will the geolocation differences be handled by changing the url? If so is adding a subdomain sufficient?

Example: [https://www.us.rhythmlivin.com/](https://www.us.rhythmlivin.com/) , [https://www.eu.rhythmlivin.com](https://www.eu.rhythmlivin.com) , [https://www.rhythmlivin.com](https://www.rhythmlivin.com) \- australia won't need it.

1. Are there any specific features which would need to be added?
    1. Different design / experience for male/females
    2. Free shipping over x amount spent.
    3. Suggest a product section?
    4. Product bundles
    5. Product Subscriptions
    6. Advanced account interface \(other than the default Shopify\)
    7. Customer Support centre \- if you already use a service like SalesForce, is this required to be implemented?

 

 

|

|Blog

1. Will there be multiple categories \(i.e. Travel, surf,

                                                                                                                                                                                                                                                                                                                                                                                                                                   |

||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |

||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|Phase 2: Tandem Project

1. What types of products?
    1. Only 1 style of jacket?
2. Will it be 2d or 3d?
    1. Who will provide the models?
3. Responsive \- will this work exactly the same on mobile as desktop?
4. Once the product is ordered, how will this information be sent to the manufacturer? \(through an Inventory system, or something else?\)
5. Do you have examples in mind? These are two Shopify apps that I think may be suitable from the brief scope I have: expivi and Zakeke

|||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |

||

 

Created with Microsoft OneNote 2016.
