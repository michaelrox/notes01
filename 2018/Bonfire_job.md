# Bonfire job

* **Are you a highly proficient wordpress/PHP developer with solid design skills?�**�
* **Do you have a keen interest in modern web technologies and applying them to create "Awesome" websites?�**�
* **Do you have an impressive folio of WordPress websites that highlights your prowess?**

If that's you – read on ... we promise not to bore you with typical seek ad blah blah blah ...

 

**About the role**:

You will be working with a creative director, designer and project manager to produce awesome WordPress websites.

 

**In terms of your prowess – here's what you MUST have:**

* Minimum of 2 years experience in producing amazing WordPress websites
* Strong knowledge of PHP & CMS/MySQL
* Understanding wordpress plugins and the wordpress environment
* HTML5, JavaScript/jQuery and CSS3
* Knowledge of Adobe Photoshop and Illustrator \(design skills also desirable\)
* Ability to slice and optimise images
* Strong Media Query & Responsive/Mobile design skills
* Cross\-Browser Testing
* Implementing payment gateways, shop modules & API's
* Experience with hosting/email environments and support
* Good understanding of web technologies and emerging trends
* Ability to work to deadlines and manage your time
* Excellent communication skills.

 

**About us**... we're Bonfire and have been in the digital marketing industry since 1996.

* We deliver client results that are second to none
* Our focus is on outcomes that grow our clients' businesses – not just sound good on paper
* We're not your standard agency. We're pragmatic, business savvy and equal parts hip and square\!
* We are Perth's leading digital agency with 26 staff and growing\!

**What you can expect from us:**

* A fast growing and profitable company
* Deep values around customer focus, premium deliverables, high performance and investment in the business by the business.
* Support and mentoring from the team of some of Australia's top digital professionals.
* A "relatively" young, inspired and extremely motivated team
* Endless Coffee – Nespresso
* Flexible working hours
* A trendy, newly designed office in Nedlands \(soon to be Subiaco\), close to public transport, cafes, eateries, and all the usual must haves.

A salary will be negotiated accordingly with the right individual based on what they can bring to the table.
