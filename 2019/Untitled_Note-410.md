# Untitled Note

`Personal`

5/3 recap

Tuesday, 12 March 2019

8:49 AM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |

|

|
|-|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

|Schedule

* Weekly Dev chat
* 3 \+ 6 month review
* Monthly check\-in

 

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |

||
| 

| Goals:

 Solid delivery of projects

* On time
* To budget / profit
* Client satisfaction

* Building infrastructure, processes & organisation along the way
    * Pitch
    * Scope 
    * Plan 
    * Execute 
    * Manage
    * Presentations, knowledge, relationships
* Building deep knowledge of Shopify and Shopify plus
    * Limitations
    * Best in class
    * Apps
    * Future & innovation
    * Competitors
* Building eCommerce knowledge
    * Best in class
    * Navigation product pages
    * Cart
    * Mobile experience
* Forming an opinion on the growth of the department and resourcing
    * P&L accountability
    * Team
    * Resource & skill \(domestic / international\)

Here's what's next

1. We meet with a summarised version 
2. Agree 3 \+ 6 month deliverables
3. Agree monthly check in

|

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |

||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

|Key Performance indicators

Separate Internal and client side

* Knowledge
* Quality
* Leadership
* Communication
* Dependability
* Punctuality
* Training

 

Internal

* Knowledge
    * Build a library of information for Periscope staff to use and access easily.

Periscope knowledgebase deliverables

1. Generic template with Title page, header and footer.
2. eCommerce platform choice list
3. Project Plan
4. Quality Assurance check lists
    1. Pre go\-live
    2. Post go\-live
5. Proposal doc

 

* Build personal knowledge and be accessible for Periscope staff.

* Quality
    * Maintain a standard that Periscope is proud to put their name to.
* Leadership
    * Take initiative
    * Manage external stakeholders like Shaligram
    * Recruitment in Periscopes web team
    * Development of new office and culture
* Communication
    * Take initiative to help team members
    * Be accessible to team members
    * Help team mates to reach targets
    * Communicate with external stakeholders like Shopify, PayPal, Shaligram etc.
    * Build network of external connections
* Dependability
    * P&L accountability
* Punctuality
    * On time for work and transparent with outliers.
    * Tasks done on time
* Training
    * Personal research, also done in own time:
        * Website platforms
        * eCommerce trends
        * Project Management trends
    * Attend conferences/meetings/communities

 

Client side

* Knowledge
    * Build a library of documents for clients to access.
    * Know answers and options for all client questions.
* Quality
    * Maintain a standard that Periscope is proud to put their name to.
* Leadership
    * Manage clients time, costs and expectations.
* Communication
    * Take initiative to help clients where I can
    * Be accessible to client
    * Build professional relationships
    * Help clients to reach targets
* Dependability
    * Be the safety net for clients, if something fails then I'll be there to fix it.
    * Be ahead of problems and bugs
    * Under promise, over deliver
* Punctuality
    * Milestones achieved on time when within my power
* Training
    * Personal research, also done in own time:
        * Clients interests \(i.e. Surf boards, Lego.\)
        * Clients platforms and options \- even if they aren't on something we currently support.
    * Engage in client related communities and events

|||
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |||

 

Created with Microsoft OneNote 2016.
