# Untitled Note

`Tasks`

Style Runner research

Monday, 19 August 2019

1:44 PM

| 

|

                                                                                                                                            |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              |

|
|-|--------------------------------------------------------------------------------------------------------------------------------------------||--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||
| 

|BuiltWith:

* Segment data management
* Google Optimize 360
* Double Click
* LiveChat
* MultiCurrency
* NetSuite CRM
* GoDaddy DNS
* NetSuite Hosting

|

|About

Stylerunner is the first online store to specialise in premium international activewear labels. With over 20 brands including celebrity favourites \- adidas by Stella McCartney, Beyond Yoga, Lisa Marie Fernandez, Lucas Hugh and Under Armour \- the range has been carefully curated to bring together an edit of must\-have workout wear, all in one place.

 

Stylerunner believes in enjoying an active lifestyle, and is empowering women around the world to feel great and perform at their best.

 

Founded by twin sisters, Julie and Sali Stevanja, Stylerunner is Australian owned and operated, offering 3 hour delivery in Sydney Metro and express shipping in Australia and Internationally.

 

||
| 

|

                                                                                                                                            |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ||
| 

|More about:

* Aussie \- NSW
* Founded 2012

 

 

                                                                                                  |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ||
| 

|

                                                                                                                                            |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              ||

| 

|

                                                                                                                                                                                                                                                                                                                                                          |

|

                                                                                                                                                                                                                        |
|-|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|Website analysis:

Home Page:

* Vertical spacing inconsistent
* Fonts blury in some areas
    * Due to using images where text should be
* Cookie notice not needed outside EU
* Dropdown menus expect to be hover not click

 

Collection pages

* Hover image of action is nice \- inconsistent though

 

Product

* Seen on the gram doesn't link or provide real data.

 

|

|Quick Lighthouse check \(on pdp\):

* Performance:
    * 93
        * Images can be compressed

 

* Accessibility
    * 66 \- some basic html structure
* SEO
    * 73
        * No description\!
        * Some other basic stuff

 

 

|
| 

|

                                                                                                                                                                                                                                                                                                                                                          |

|                                                                                                                                                                                                                        |

 

Created with Microsoft OneNote 2016.
