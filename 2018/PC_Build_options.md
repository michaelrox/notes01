# PC Build options

**GumTree**

[http://www.gumtree.com.au/s\-ad/queens\-park/desktops/gaming\-pc\-for\-sale\-includes\-peripherals\-/1093723096](http://www.gumtree.com.au/s-ad/queens-park/desktops/gaming-pc-for-sale-includes-peripherals-/1093723096)
$950
CPU: AMD FX 4300 @ 3.8 GHZ
Graphics Card: Asus R9 270 OC GB GDDR5
PSU: Cooler Master 600W G Series 80\+ Bronze Certified \(Non\-Modular\)
Motherboard: ASUS M5A97 R2.0
RAM: Kingston HyperX 8GB \(2X 4GB Sticks\) 1600 MHz
HDD: Seagate Barracuda 1TB
Optical Drive: Samsung Internal Optical Drive
Case: Corsair Carbide 200R
Monitor: Viewsonic VX2240w
Keyboard & Mouse: CoolerMaster Devastator Bundle
Headphones: Corsair Vengeance 1300 Analog Gaming Headset
Wi\-Fi Adapter: NETGEAR AC600 Dual Band Adapter

![fd975ae8-9946-45d0-85ca-a1f29727412f.png](image/fd975ae8-9946-45d0-85ca-a1f29727412f.png)

\- how big is the Samsung  Internal Optical Drive?
\- Does it come with Windows?
