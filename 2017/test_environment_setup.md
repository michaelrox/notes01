# test environment setup

plan

* ~~setup git~~
* ~~duplicate theme~~
* ~~download all files with motif~~
* ~~once downloaded \- re\-download the _Black Friday Update_ theme from shop~~
    * ~~in to hismile2017 folder~~
* ~~git push new hismile2017 folder~~
* ~~test...~~
* ~~test with dummy live theme~~

environment

local env \-\> beta environment \-\> live environment

local \- motifmate

beta \- hismile\-dev.myshopify.com

live \- hismileint.myshopify.com

process

Local process

1. git pull
2. open Motifmate
3. Work on theme locally \_dev Motifmate

Beta test process

1. gitpush to develop branch
2. use Motifmate to push theme to \_dev version
3. set up the customize settings to be 100% accurate on beta
4. send for test approval

Deploy process

1. Download the theme using shopify
2. Upload the theme to live
3. Adjust settings

Sourcetree

\- gitpush to backup

npm updates:

minimatch

'npm ls graceful\-fs'

lodash

links

[http://motifmate.com/documentation/browsersync](http://motifmate.com/documentation/browsersync)

[https://apps.shopify.com/motifmate](https://apps.shopify.com/motifmate)

[https://hismile\-dev.myshopify.com/admin](https://hismile-dev.myshopify.com/admin) \- my dev playground...not sure how to use yet

Migrate?

[https://ecommerce.shopify.com/c/shopify\-discussion/t/migrate\-live\-store\-to\-dev\-store\-possible\-152563](https://ecommerce.shopify.com/c/shopify-discussion/t/migrate-live-store-to-dev-store-possible-152563)

2013: PareidoliaX \- If you're just trying to export the theme from your live store and then have it carry over to your dev account so you can keep working away at it, getting your theme exported from Shopify is a piece of cake\! This article will show you how to import/export themes between shops. website/5\-themes/can\-i\-download\-and\-upload\-a\-theme"\>http://docs.shopify.com/support/your\-website/5\-themes/can\-i\-download\-and\-upload\-a\-theme

In order to carry over your products and customers you can basically follow the same steps. From both the "Products" and "Customers" pages, you can export a .CSV file containing all the information from your live store. Then open up your dev shop, import those .CSV files, and now you have a dev shop with most of the data from your live store.

Unfortunately, you're only going to be able to use these steps for your theme, products, and customers. You won't be able to import orders, and other settings like shipping rates, discounts, link lists, etc. \- those will have to be added manually. Hope this helps\!

Help

Hey, I'm wondering if anyone can help me with setting up a local shopify theme development environment. My employer has shopify plus, and has about 5 different stores for different locations which all use the same theme.

My current process doesn't seem very smooth, I'm used to working locally and pushing to a beta/development server using git, which can then be push to a live site again using git. Particularly when publishing the changes live, across 5 sites, I feel like I'm setting myself up for error.

Basically, my process is:

1. pull latest changes \(Sourcetree/bitbucket\)

2. Work on theme locally \(Motifmate\)

3. Synchronize to test in editor of the live site \(Motifmate\)

4. push to backup \(manually pushing just for the sake of verison control\) \(Sourcetree/bitbucket\)

5. Publish for go live \(Motifmate\)

6. Download theme, upload manually to other sites \(shopify\)

I'd really appreciate any advice or links for. I have found it hard to get reliable information on this topic as Shopify makes big changes all the time.

The greatest help would be a straight bullet point list as I have done, and if you can mention the software you use then I can do the research myself on how to set it up.
