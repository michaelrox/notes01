# Mothers Day

**Card from me:**

Dear Chantelle,

I'm so proud of you for overcoming so much along this journey, and the person you have come to be along the way.

It brings me joy to know that the way you make others feel safe, supported and cared for will be reflecting on Kian as he grows.

I'm so grateful to call you the mother of my son.

**Card from Kian:**

Dear mum,

You've carried me on such a long journey, surrounding me with feelings of safety, love, support and more love\!

You are the first friend I've ever had, and the best friend I'll ever have\!

Love always\! 

Kian

**Instagram post**

Kian is so fortunate to have a mother who provides so much care, support and love towards those around her.

 

Thinking of you as the mother of Kian will always bring me an unforgettable rush of uplifting emotions.

I'm so grateful to call you the mother of my son.

**another time:**

Kian's birth will always be a part of you and your story as a mother. A story which you own, and which you know will be fully supported.

To the world you are a mother, but to us you are the absolute world.

"Until you are a parent" started as a joke but is now a serious part of our day to day mentality.
Just like the phrase "You entered labour as a women, and came out a mother."

I believe it is how the woman is treated, how safe she feels, and how supported she is, that ultimately makes the memories of baby’s birthday uplifting and unforgettable.

Our journey began working at McDonalds serving french fries. It was about time we decided to upsize to a family meal.
