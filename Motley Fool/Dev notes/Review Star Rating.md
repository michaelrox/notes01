# Review Star Rating
* Add ACF field here : [ticker.php](https://github.com/themotleyfool/wp-freesite-2020-theme/blob/master/inc/acf-fields/ticker.php) 
* In this example, adding the code below after the addWysiwyg() field  

``` 
->addText(
  'company_star_rating',
  [
	  'key' => 'field_5a7ed5380b5e1',
	  'label' => 'Company Star Rating',
	  'name' => 'company-star-rating',
	  'instructions' => 'Input the star rating as a number between 0 and 200.',
  ]
)
		  
```

* Read more about this : [here](https://github.com/StoutLogic/acf-builder)

---

* Add a function to [financial-pages.php](https://github.com/themotleyfool/wp-freesite-2020-theme/blob/master/inc/financial-pages.php)

```

add_action('wp_head', function () {
    if (isTickerPage()) {
        $ticker = get_queried_object();
        if ($ticker && get_field("company-star-rating", $ticker->taxonomy . "_" . $ticker->term_id)) {
            $tickerURL = get_term_link($ticker);
            $rating = get_field("company-star-rating", $ticker->taxonomy . "_" . $ticker->term_id);

            echo '
                <script type="application/ld+json">
    									{
    									  "@context": "http://schema.org/",
    									  "@type": "Corporation",
    									  "name": "' . $ticker->company_name . '",
    									  "tickerSymbol": "' . $ticker->exchange . ':' . $ticker->symbol . '",
    									  "alternateName": "' . $ticker->symbol . '",
    									  "url": "' . $tickerURL . '",
    							          "Review": [{
    							          "@type": "Review",
    							          "additionalType": "http://www.productontology.org/id/Fundamental_analysis",
    							          "author": {
    							              "@type": "Organization",
    							              "name": "The Motley Fool"
    							              },
    							          "reviewRating": [{
    							            "@type": "Rating",
    							            "ratingValue": ' . $rating . ',
    							            "bestRating":200,
    							            "worstRating": 0
    							            }]
    							          }]
    							        }
    						</script>
                ';
        }
    }
}, 1);

```