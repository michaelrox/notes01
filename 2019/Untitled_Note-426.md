# Untitled Note

`Process`

Kickoff

Friday, 22 February 2019

2:40 PM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
|-|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|External

1. Introductions – some warm and fuzzy banter \(5 mins\)
2. Review the project teams – who’s responsible for what? \(3 mins\)
3. Approval process – the process and personnel for signing off deliverables? \(3 mins\)
4. SoW Review – what are we doing, when, how, and what will we produce? \(20 mins\)
5. Discuss Risk, Issue and Change Management – what’s the client’s attitude and approach to manage to risk and change? \(3 mins\)
6. Reporting – how will we track and communicate project progress, and to whom? \(3 mins\)
7. Collaboration – what tools will we use to work together? \(3 mins\)
8. Assets – what do we need to get started? \(5 mins\)
9. Kickoff agenda – what will we discuss in the client kickoff? \(5 mins\)
10. AOB – anything else that we need to discuss? \(5 mins\)

 

From \<[https://thedigitalprojectmanager.com/project\-kickoff\-meeting/](https://thedigitalprojectmanager.com/project-kickoff-meeting/)\>

|
| 

|Internal

1. Introductions – meet your new best buds \(15 mins\)
2. Client – what’s the background? \(5 mins\)
3. Project – why are we doing this? \(5 mins\)
4. Scope – what are we doing? \(20 mins\)
    1. Timeline
    2. Estimate
    3. SoW
    4. RAID \(Risks, Assumptions, Issues and Dependencies\) log
5. Approach – how are we going to make this happen? \(20 mins\)
6. Roles – who is doing what? \(5 mins\)
7. Teamwork – how are we going to work together? \(5 mins\)
8. Kickoff – what’s the agenda for the client kickoff? \(5 mins\)
9. Next – how do we keep momentum? \(5 mins\)
10. Q&A – what haven’t we told you? \(5 mins\)

 

From \<[https://thedigitalprojectmanager.com/project\-kickoff\-meeting/](https://thedigitalprojectmanager.com/project-kickoff-meeting/)\>

 

 

|

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |

 

Created with Microsoft OneNote 2016.
