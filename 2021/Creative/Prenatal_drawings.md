# Prenatal drawings

Publish around week 33 | February 3

\- 7 weeks to go

\- month 8

**Baby differentiates day from night**

If your uterus had eyes, here's what you'd see: your fetus acting more and more like a baby, with his eyes closing during sleep and opening while awake.

And because those uterine walls are becoming thinner, more light penetrates the womb, helping your baby differentiate between day and night. Now if only baby can remember that difference on the outside\!

**Fetal immune system is developing**

Good news\! Your baby has reached an important milestone about now: He's got his own immune system. Antibodies are being passed from you to your little one as he continues to develop his fetal immune system, which will come in handy once he's outside the womb and fending off all sorts of germs.

**HypnoBirthing**

\- A focus on positive thoughts and words

\- Controlled breathing

\- Guided visualization

**Week 34**

**Thick skin**

The waxy, cheesy coating on your baby’s skin — vernix — begins to thicken this week before it starts shedding in the next few weeks.

**Week 35**

**Padding up**

The majority of your baby’s growth now is fat. His shoulders in particular are padding up for the journey south.

****Week 36

Nesting Instinct****

Reorganizing the closets, alphabetizing the spice rack and thwarting dust\-bunny breeding efforts under the bed? Welcome to a pre\-labor ritual that helps get your home ready for the baby — and helps you pass the time.

**Week 37**

**\-**

**Week 38**

**Shedding hair**

The lanugo, the fine downy hair that covered your LO's body for warmth, is falling off in preparation for delivery.

\- Month 9

\- Prepare post\-natal foods

**Week 39**

\- Big brain: Brain grew 30% in a week

\- Skin turns from pink to white

\- Full term

\- 

**Week 40**

Babies eyesight is all blurry

\-\-

**Words:**

Let Go

Surrender

******\-\-\-******

********Colours:

Wood / Oak \- match buffet********
