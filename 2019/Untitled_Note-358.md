# Untitled Note

`Mutt`

CRM

Thursday, 9 May 2019

7:47 AM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

|

                                                                                                                                                                                                                                                                        |
|-|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

|I'm acting as a consultant for a company who is currently on HubSpot, but wants to start tracking leads from social marketing. This upgrade costs a lot with HubSpot and I don't see them using a lot of the extra features that come with HubSpot at the upgrade price.

|
| 

| 

 

Contacted:

* Hubspot
    * $1,050 / m / u
    * Would include email marketing
    * Looks nice
* Green Rope
    * Can do everything else
    * A little ugly
    * Higher price
        * $519 installation, including first month
        * $260/m after
    * Not as detailed analytics
* Hatchbuck
    * No option
    * Lots of small costs
* Solve360
    * Can grab utm links
    * Relys on WuFoo forms \(additional cost\)
* Salesforce
    * $1,500 monthly
    * Installation fee is a lot more
* Apptivo
    * $1,000 \- $1,500
    * No analytics
    * No integration \- requires csv imports every time
* Zoho
    * Most affordable
        * $44 / m / u
    * Does it all
    * Support is all Indian on phone/chat
    * Support is 24hrs 5 days
    * No set up fees

|

|                                                                                                                                                                                                                                                                        |
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |

|                                                                                                                                                                                                                                                                        |

| 

|

                                                                                      |

|

                                                                                                                                                                              |
|-|--------------------------------------------------------------------------------------||------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|

                                                                                      |

|Tim uses Hubspot for sales funnel

1. Enquiry \- pretty keen etc.
2. Tracks email and shows when opened and how many times
3. Solve360 is around $100usd/month

AirTable integration

|
| 

|Salesforce

$35 user / month

 

Lots of cost

Complex emails

Tracking journeys

Integrations

 

 

 

 

|

|                                                                                                                                                                              |
| 

|

                                                                                      |

|                                                                                                                                                                              |

Primary needs:

* Internal and external stakeholder groups \- leads, internal teams etc.
* Email campaigns
* Tracking social marketing campaigns
* Tracking Phone calls \- preferably integrate with Jet Interactive
* Sign\-up form that is able to be embedded and pre\-populated based on either user history or query string
* Shopify and AirTable integration a bonus
* Easy support \- we are based in Australia and don't usually like bots.

 

 

Created with Microsoft OneNote 2016.
