# September 2017

* Work
* Gym / health
* House
* Friends 
* Chantelle 
* Life

Work

* Shit office \- tight, smells, cheap
* Bad location,  far away from home 
* Bad people,  bitching, no truth, trashy 
* Company isn't transparent 
* Bad client relationships 
* Loud,  screechy loud bitching

Gym / health 

* Motivation 
* I'm a long way off my records
* Not happy with my body 
* Gym has bad shower 

House

* Cheap
* Not comfortable 
* Bad for guests 
* Wardrobe 
* No living room 

Friends 

* All in Brisbane 
* No motivation to make new 
* Want someone to spend an hour a day with to talk about stuff like this with 

Chantelle 

* I'm always letting her down 
* I can't keep up 
* I ask too much... 
* I'm afraid I'll lose her 

Life

* Can't keep up 
* Enjoy my vices too much 
* Improving above takes time, which I struggle with 
* Life's not worth living if I can't do what I want... 
* Even though everything is borderline perfect in my life I still aren't happy... 

---

Solutions... 

Work

* Headphones 
* Work from home
* Don't care about company 
* Do work, go home...

Gym

* Forget goals, just spend time there to build routine. 
* Test night training for a month, this will also mean less gaming which may help 
* Set tiny tiny goals

House

* Move desk to garage
* Buy living room stuff 
* Spend time with Chantelle 

Friends 

* Call family and friends often 

Chantelle 

* Do dishes 
* Spend time with her 

Life

* Improve slightly 
* Do tax
* See dentist
* Call family and Luke often 
* Give 1 hr a day for all vices
