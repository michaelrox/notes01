# Becoming a mother

Birth is a transformative experience for women, regardless of how it happens. 

You will literally never be the same again.

 You go into labour a woman, and come out the other end a mother. 

All women want to have a positive birth experience. So, what is it exactly that makes for a good experience? 

It’s not necessarily the type of birth. 

I believe it is how the woman is treated, how safe she feels, and how supported she is, that ultimately makes the memories of baby’s birthday uplifting and unforgettable.

 

Because let’s get real \- birth rarely goes completely to plan. Both my births \- the first a breech vaginal birth in hospital, the second a fast and furious homebirth, proved to me that birth is an unpredictable force of nature. It can’t be controlled, it can’t really be planned. All we can do is get our head into the right place \- knowing the outcome we want, focusing on it and using it as motivation to power through... but knowing that sometimes baby has other plans. Recognise that one of the greatest aspects of the birth process that you can influence and the one over which you have total power, is how you take on and process the experience that is given to you, whether that is the home water birth, or the emergency caesarean section. It may be that you didn’t get much of a choice in how it played out, but you do get a choice in how you let your story affect you as you move forward in life. For anyone whose birth wasn’t the positive experience they hoped for, you will be familiar with the common attitude in our culture that women ‘shouldn’t get their hopes up for a positive birth and that they should just be grateful to come out of it with a healthy baby’. This attitude may be well intentioned, but as a midwife and as a mother myself, I have serious concerns about it. Birth is a time of extreme physical and emotional vulnerability for women and the partners who are supporting them. It is truly a rite of passage and to minimise that is unfair and detrimental to these women’s mental health. 

So the most important message to take home after giving birth is that however it went, whether it was to plan or not… how you FEEL about the process is important and absolutely, completely valid. 

It’s OK to be proud of yourself and your body. It’s OK to feel ecstatic and empowered. It’s OK to be in awe of your body. It’s OK to want to share your story and to view it as an accomplishment.

 BUT: It’s also OK to be disappointed if your birth plan didn’t work out. It’s also OK to be angry if you felt disrespected by the people around you. It’s also OK to be shocked if it was far more intense than you anticipated. It’s also OK to have regrets and to wish things had gone differently, even if your baby is safe in your arms. 

Your baby’s birth will always be a part of you and your story as a mother. And you already own that story, but you don’t need to do it alone. If you need help unpacking your birth, the following resources are a great place to start:
