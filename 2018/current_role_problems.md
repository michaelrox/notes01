# current role problems

My current role doesn't offer much motivation, being options for career progression, salary increase, challenges, or rewarding work. I brought these points up with my current employer a few months ago and in compromise I was given permission to work from home 4 days a week. 

The thing keeping me here is the extremely relaxed work environment and low expectations \(I'm working from home, no time\-frames for projects, no supervision etc.\)

I initially thought I could live fine with this easy work life\-style and it would be a great environment for when I think about having kids in the future, but in the end it has made me even more unmotivated.
