# Untitled Note

`Tasks`

Job Description

Tuesday, 5 March 2019

10:44 AM

| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|-|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------||------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

|
    * A minimum of 3 years experience in a similar eCommerce / website development project management role.
    * A comprehensive understanding of eCommerce technology & platforms.
    * Experience in successfully managing development and technology projects with multiple stakeholders.
    * Experience in the Shopify & Shopify plus / Magneto / Woo commerce platforms.
    * Experience in or good knowledge of the delivery and management process’s associated with website development.
    * A basic understanding of HTML, XML and web integrations.
    * A natural interest digital, technology and innovation.
    * Excellent communication skills
    * Direct experience working on / with an eCommerce businesses.
    * Energy to broaden your digital knowledge and skillset.
    * An ambition to develop, learn & grow \(quickly\)
    * Boundless energy & enthusiasm. \(We're a start\-up, so fasten your seatbelt\)
Some of your key responsibilities will be:
    * Lead the project management of eCommerce platform development including replatforming, upgrades / enhancements and eCommerce effectiveness auditing.
    * Create detailed project plans, timelines and quote documentation.
    * Build expert internal knowledge, expertise and capability on eCommerce platform technology, development and integrations.
    * Effectively communicate with and manage internal & external stakeholders to ensure projects are delivered on time and to specification.
    * Record and manage your personal and your team’s time to ensure projects are delivered in line with budgets.
    * Identify revenue / profit growth opportunities with clients that will grow & develop PeriscoPe’s relationship and services.
    * Use analytics technology to identify opportunities and conduct post project completion effectiveness.
    * Keep detailed notes on all internal and external meetings.
    * Develop, lead & execute on quality assurance standards and processes.
    * Build & lead a team of trusted internal / contractual developers to deliver projects.
If you think you've got the skills that we're looking for get in touch with us as quick as you can at[hello@thisisperiscope.com](mailto:hello@thisisperiscope.com)

|
| 

| 

Experience/skills

|Design/Dev

  |1

|
|------------|-|
|Research

    |2

|
|Presentation

|3

|
|Support

     |4

|
|eCommerce

   |5

|
|Shopify

     |6

|

 

Interests:

Surf

Motorbikes

Travel / outdoors

Family

 

\-\-\-\-\-\-\-

 

Key Skills:

 

* eCommerce website development and design.
* An understanding of HTML, CSS and basic JS.
* A solid understanding of eCommerce technology & platforms.
* A natural interest digital, technology and innovation.
* Excellent communication skills
* An ambition to develop, learn & grow \(quickly\)
* Boundless energy & enthusiasm. \(We're a start\-up, so fasten your seatbelt\)
* Ability to communicate with clients on a presentation level as well as on\-going support and communication.

 

Key Responsibilities

* Coordinate and implement development for replatforming, and website updates
* Communicate with and manage external stakeholders.
* Build, test and sign off on external stakeholders.
* Use analytics technology to identify client opportunities for growth.
* Multi\-task between multiple clients

 

Bonus skills:

* Experience in the Shopify & Shopify plus / Magneto / Woo commerce platforms.
* Knowledge of User Experience design
* An interest in anything that can help spark relationships with existing or potential clients. Currently ranging across surf, motorbikes, music, travel, outdoors, family, fashion, partyware.

 

 

 

If you think you've got the skills that we're looking for get in touch with us as quick as you can at [hello@thisisperiscope.com](mailto:hello@thisisperiscope.com) 

 

 

 

 

 

 

 

 

 

|

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
| 

|

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |

|                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |

 

Created with Microsoft OneNote 2016.
