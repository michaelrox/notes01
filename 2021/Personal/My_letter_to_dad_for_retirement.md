# My letter to dad for retirement

I hope you share the pride I feel in having parents who have accomplished so much in their lives.

The hard work you have done, the leadership you have provided, and the great difference you have made have all shone through in your parenting and are reflected on those around you.

Your retirement comes at a time where all of your kids are embracing new milestones in their own lives. I'm excited to see as you continue to provide guidance and pass down your life learned knowledge.

You deserve a long and happy retirement after making such a difference.
