# Kian Astrology

**6th House**

**Zodiac sign Aries**

Straightforward, helpful, objective and strong\-willed

Born to be active

**Scorpio Ascendant**

Can develop great passion \- hobby, love, life goal.

Moon sign Capricorn \- in the 2nd House

Emotional self is somewhat repressed in the name of responsibility, seriousness, and rationality. You crave the guidance and comfort of a teacher or parent. It's in your second house, meaning you find security and safety through money and material possessions.
